<?php

class Suo_Model_RoomGateway extends Suo_Model_Base_DataGateway
{
    protected $_data_name = 'Room';

    public function fetchRoomsByIdsSortedByNumber(array $ids)
    {
        $table = $this->getTable($this->getName());
        $select = $table->select()
            ->from($table, array('*', "LPAD(`number`, 10, ' ') AS room_number"))
            ->where('id IN (?)', $ids)
            ->order('room_number');
            //throw new Exception(var_export($select->__toString(), true));
        $records = $table->fetchAll($select);
        $set = new Suo_Model_RoomSet($records, $this);
        return $set;
    }

    public function fetchRoomsByPanel($panel_address)
    {
        $db = Zend_Registry::get('db');
        $query = "
SELECT `room`.`id`, `room`.`number`, `room`.`description`, `room`.`accept`, `room`.`acceptday`
FROM `room`
WHERE `room`.`id` IN (
  SELECT `panel_has_room`.`room_id`
    FROM `panel_has_room`
    JOIN `panel`
      ON `panel`.`id` = `panel_has_room`.`panel_id`
    WHERE `panel`.`netaddress`='$panel_address')

        ";

        $result = $db->fetchAll($query);

        return $result;
    }

    public function fetchRoomIdByPanelAddress($panel_address)
    {
        $db = Zend_Registry::get('db');
        $query = "
SELECT `panel_has_room`.`room_id`
FROM `panel_has_room`
JOIN `panel`
     ON `panel`.`id` = `panel_has_room`.`panel_id`
WHERE `panel`.`netaddress`='$panel_address'
";

        $result = $db->fetchAll($query);

        return $result;
    }

    public function fetchRoomsByIds($ids)
    {
        $db = Zend_Registry::get('db');
        $query = "
 SELECT `room`.`id`, `room`.`number`, `room`.`description`, `room`.`accept`, `room`.`acceptday`, `room`.`windowcount`
 FROM `room`
 WHERE `room`.`id` IN ($ids)
 ORDER BY CAST(`room`.`number` AS UNSIGNED)
";

        $result = $db->fetchAll($query);
        return $result;
    }

    public function fetchAllToOperator()
    {
        $db = Zend_Registry::get('db');
        $query = "
SELECT `room`.`id`, `room`.`number`, `room`.`description`
FROM `room`
ORDER BY CAST(`room`.`number` AS UNSIGNED)
";

        $result = $db->fetchAll($query);

        return $result;
    }

    public function setRoomAccept($room_id, $accept)
    {
        $db = Zend_Registry::get('db');
        $query = "
UPDATE `room`
SET `accept`='$accept'
WHERE `id`='$room_id'
";
        $db->query($query);
    }

    public function fetchMaxRecordByRoom($room_id)
    {
        $db = Zend_Registry::get('db');
        $query = "
SELECT `room`.`recordcount`
FROM `room`
WHERE `room`.`id`='$room_id'
LIMIT 1
";
        $result = $db->fetchAll($query);

        return $result;
    }

    public function setMaxTodayRecords($room_id, $max_records)
    {
        $db = Zend_Registry::get('db');
        $query = "
UPDATE `room`
SET `max_today_records`='$max_records'
WHERE `id`='$room_id'
";
        $db->query($query);
    }
}