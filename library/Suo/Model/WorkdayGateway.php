<?php

class Suo_Model_WorkdayGateway extends Suo_Model_Base_DataGateway
{
    protected $_data_name = 'Workday';

    public function fetchByRoomAndDates($room_id, $date_start, $date_end)
    {
        $table = $this->getTable($this->getName());
        $select = $table->select();
        $select->where('room_id = ?', $room_id)
            ->where('begin >= ?', $date_start)
            ->where('end <= ?', $date_end);
        $rows = $table->fetchAll($select);
        $result = $rows->toArray();
        return $result;
    }
}