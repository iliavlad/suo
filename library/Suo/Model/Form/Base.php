<?php

class Suo_Model_Form_Base extends Zend_Form
{
    public function __construct(Suo_Model_Base_ViewData $view_data, $options = null)
    {
        $this->_view_data = $view_data;
        parent::__construct($options);
    }

    public function init()
    {
        parent::init();
        $object = $this->getViewData();
        $properties = $object->getProperties();
        $dependent = $object->getDependent();
        $dependent_prop = array_keys($dependent);
        foreach ($properties as $name) {
            $value = $object->$name;
            if ('id' == $name) {
                $element = new Zend_Form_Element_Hidden($name, array('value' => $value, ));
            } else if (in_array($name, $dependent_prop)) {
                $element = $this->getMultipleValue($name, $value, $dependent[$name]);
            } else {
                $element = new Zend_Form_Element_Text($name, array('value' => $value, 'label' => $name, ));
            }
            $this->addElement($element);
        }
        $this->setMethod(self::METHOD_POST)
            ->addElement('submit', 'submit')
            ->addElement('reset', 'reset');
    }

    /**
     * @var Suo_Model_Base_ViewData
     */
    protected $_view_data = null;

    /**
     * @return Suo_Model_Base_ViewData
     */
    protected function getViewData()
    {
        return $this->_view_data;
    }

    protected function getMultipleValue($name, $value, $dependent)
    {
        if (isset($dependent['many'])) {
            $multi = new Zend_Form_Element_MultiCheckbox($name, array('label' => $name, ));
            $dependent_name = $dependent['many']['dependent_name'];
            $ids = array();
            foreach ($value as $data) {
                $ids[] = $data[$dependent_name];
            }
        } else {
            $multi = new Zend_Form_Element_Select($name, array('label' => $name, ));
            $ids = $value;
        }
        $gateway = Suo_Model_Base_GatewayFactory::getGateway($dependent['table']);
        $all = $gateway->fetchAll();
        foreach ($all as $data) {
            $id = $data->id;
            $multi->addMultiOption($id, $data);
        }
        $multi->setValue($ids);
        return $multi;
    }
}