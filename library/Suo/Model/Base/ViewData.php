<?php
/**
 * Класс для работы с данными, у которых есть форма для вывода
 *
 * @author Ilya Garaga <inform@rem-prim.ru>
 * @since 02.04.2009
 * @package InformClient
 *
 */
abstract class Suo_Model_Base_ViewData extends Suo_Model_Base_DataGatewayed
{
    /**
     * Получение свойств данных
     *
     * @param bool $to_display Если запрос свойств для показа, то выделяются только поля, которые может смотреть пользователь
     * @return Suo_Model_Base_Properties
     */
    public function getProperties($to_display = false)
    {
        $result = array_merge($this->_data, $this->_data_many_to_many);
        //$result = $this->_data;//array_keys($this->_data);
//        if (true == $to_display) {
//            $user_fields = $this->getGateway()->getUserFields();
//            if (null !== $user_fields) {
//                $result = array_intersect_key($result, $user_fields);
//            }
//        }
        return $result;
    }


    /**
     * @return Suo_Model_Form_Base
     */
    public function getForm()
    {
        $form = $this->_data_form;
        if (null == $form) {
            $class_name = 'Suo_Model_Form_' . $this->getName();
        $form = new $class_name($this);
            $this->_data_form = $form;
        }
        return $form;
    }

    public function getObjectUniqueFieldName()
    {
        return $this->getGateway()->getObjectUniqueFieldName();
    }


    protected $_data_form = null;
}