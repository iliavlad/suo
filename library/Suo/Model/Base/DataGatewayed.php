<?php
/**
 * Базовый класс для объектов, у которых есть шлюз для работы с ними
 *
 * @author Ilya Garaga <inform@rem-prim.ru>
 * @since 02.04.2009
 * @package InformClient
 */
abstract class Suo_Model_Base_DataGatewayed extends Suo_Model_Base_Data
{
    /**
     * Конструктор
     *
     * @param array|object $data Данные объекта
     * @param Suo_Model_Base_DataGateway $gateway Шлюз
     */
    public function __construct($data, Suo_Model_Base_DataGateway $gateway)
    {
        $this->setGateway($gateway);
        if ($data instanceof Zend_Db_Table_Row_Abstract) {
            $this->_row = $data;
            $data = $data->toArray();
        }
        parent::__construct($data);
    }

    /**
     * Получение значения свойства
     *
     * @param string $name Имя свойства
     * @return mixed Значение свойства (null, если запрашиваемое свойство не задано)
     */
    public function __get($name)
    {
        if (!in_array($name, $this->_data_many_to_many)) {
            throw new Suo_Model_Base_DataException('SUO_INVALID_PROPERTY', $name, get_class($this));
        }
        $dependent = $this->getDependent();
        $many = $dependent[$name]['many'];
        $result = $this->getGateway()->fetchDependentById($this->id, $many['table'], $many['name'],
                $many['dependent_name']);
        $this->$name = $result;
        return $result;
    }

    public function __toString()
    {
        return $this->_data['id'];
    }

    /**
     * Установка шлюза
     *
     * @param Suo_Model_Base_DataGateway $gateway Шлюз
     * @return Suo_Model_Base_DataGatewayed Этот объект
     */
    public function setGateway(Suo_Model_Base_DataGateway $gateway)
    {
        $this->_gateway = $gateway;
        return $this;
    }

    /**
     * Получение шлюза
     *
     * @return Suo_Model_Base_DataGateway Шлюз
     */
    public function getGateway()
    {
        return $this->_gateway;
    }

    /**
     * Сохранение текущего объекта
     * Обновление или вставка в базу
     */
    public function save()
    {
        $gateway = $this->getGateway();
        $dbTable = $gateway->getTable($this->getName());

        $row = $dbTable->find($this->id)->current();
        if (!$row) {
            $this->insert($dbTable);
        } else {
            $this->update($row);
        }
    }

    private function insert($table)
    {
        // выбираем данные, которые будут вставлятся в основную таблицу и дополнительные
        $main_table_data = array();
        foreach ($this->_data as $key) {
            $main_table_data[$key] = $this->$key;
        }
        unset($main_table_data['id']);
        // обрабатываем данные перед вставкой
        $this->beforeInsert($main_table_data);
        $table->insert($main_table_data);
        // TODO Посмотреть, почему не ид не возвращается методом insert
        $id = $table->getAdapter()->lastInsertId();
        $this->id = $id;
        // сохраняем данные в зависимые таблицы
        $this->updateMany();
    }

    private function update($row)
    {
        //$dependent = array();
        foreach ($this->_data as $key) {
            $row->$key = $this->$key;
        }
        // обрабатываем данные перед сохранением
        $this->beforeSave($row);
        // сохраняем данные в текущую таблицу
        $row->save();
        // сохраняем данные в зависимые таблицы
        $this->updateMany();
    }

    private function updateMany()
    {
        $many = $this->getManyToMany();
        if (!empty($many)) {
            $gateway = $this->getGateway();
            $dependent = $this->getDependent();
            foreach ($many as $property) {
                $value = $this->$property;
                $gateway->updateMany($this->id, $value, $dependent[$property]['many']);
            }
        }

    }

    public function delete()
    {
        $gateway = $this->getGateway();
        $dbTable = $gateway->getTable($this->getName());
        $num_rows = $dbTable->delete(array('id = ?' => $this->id, ));
        return $num_rows;
    }

    public function getName()
    {
        return $this->getGateway()->getName();
    }

    public function getRow()
    {
        return $this->_row;
    }

    /**
     * Шлюз
     *
     * @var Suo_Model_Base_DataGateway
     */
    protected $_gateway;

    /**
     * Если объект создается на основе данных из таблицы, то сохраняем значение строки,
     * чтобы для зависимых свойств иметь возможность выбирать данные из базы
     *
     * @var Zend_Db_Table_Row_Abstract
     */
    protected $_row = null;

    protected $_many_to_many = array();

    protected function beforeSave(Zend_Db_Table_Row_Abstract $row)
    {
    }

    protected function beforeInsert(array &$data)
    {
    }
}