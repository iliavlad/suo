<?php

class Suo_Model_Base_GatewayFactory
{
    /**
     *
     * @param string $name
     * @return Suo_Model_Base_DataGateway
     */
    public static function getGateway($name)
    {
        if (!isset(self::$_list[$name])) {
            $gateway_class = 'Suo_Model_' . $name . 'Gateway';
            self::$_list[$name] = new $gateway_class();
        }
        return self::$_list[$name];
    }

    private static $_list = array();
}