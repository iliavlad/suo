<?php
/**
 * Класс исключений при работе с классом Base_Data
 *
 * @author Ilya Garaga <inform@rem-prim.ru>
 * @since 27.03.2010
 * @package SuoLibrary
 *
 */
class Suo_Model_Base_DataException extends Suo_Exception
{
    const SUO_PROPERTY_DATA_SHOULD_BE_SET    = 1;
    const SUO_INITIAL_DATA_SHOULD_CONTAIN    = 2;
    const SUO_INVALID_PROPERTY               = 3;
    const SUO_UNIQUE_PROPERTY_NOT_SET        = 4;

    protected $_messages = array(
        self::SUO_PROPERTY_DATA_SHOULD_BE_SET => 'Свойство $_data должно быть установлено в классе %s.',
        self::SUO_INITIAL_DATA_SHOULD_CONTAIN => 'Начальные данные должны быть массивом или объектом, получено %s.',
        self::SUO_INVALID_PROPERTY            => 'Некорректное свойство %s в классе %s.',
        self::SUO_UNIQUE_PROPERTY_NOT_SET     => 'Начальные данные должны содержать значение для свойства %s.',
    );
}

/**
 * Класс для работы с данными
 * Используется для установки свойств, проверки необходимых свойств.
 *
 * @author Ilya Garaga <inform@rem-prim.ru>
 * @since 27.03.2010
 * @package SuoLibrary
 *
 */
abstract class Suo_Model_Base_Data
{
    /**
     * Конструктор класса
     *
     * @param array|object $data Данные класса
     */
    public function __construct($data)
    {
        $this->init();
        foreach ($this->_data as $property) {
            $this->$property = null;
        }
        $this->populate($data);
    }

    /**
     * Установка начальных данных
     * Также проверяется, что переданные в конструктор данные можно привести к массиву
     *
     * @param array|object $data Данные класса
     * @return Inform_Model_CommonData
     * @throws Suo_Model_Base_DataException
     */
    public function populate($data)
    {
        if (!isset($this->_data)) {
            throw new Suo_Model_Base_DataException('SUO_PROPERTY_DATA_SHOULD_BE_SET', get_class($this));
        } else if (is_object($data)) {
            $data = array($data);
        }
        if (!is_array($data)) {
            throw new Suo_Model_Base_DataException('SUO_INITIAL_DATA_SHOULD_CONTAIN', gettype($data));
        }
        foreach ($data as $key => $value) {
            $this->$key = $value;
        }
        return $this;
    }

    /**
     * Установка значения свойства
     *
     * @param string $name Имя свойства
     * @param mixed $value Значение свойства
     * @return void
     */
    public function __set($name, $value)
    {
        if (!in_array($name, $this->_data) && !in_array($name, $this->_data_many_to_many)) {
            throw new Suo_Model_Base_DataException('SUO_INVALID_PROPERTY', $name, get_class($this));
        }
        $this->$name = $value;
    }

    /**
     * Получение значения свойства
     *
     * @param string $name Имя свойства
     * @return mixed Значение свойства (null, если запрашиваемое свойство не задано)
     */
    public function __get($name)
    {
//        if (!in_array($name, $this->_data)) {
            throw new Suo_Model_Base_DataException('SUO_INVALID_PROPERTY', $name, get_class($this));
//        }
//        return $this->$name;
    }

    /**
     * Проверка на установку свойства
     *
     * @param string $name Имя свойства
     * @return boolean True, если свойство установлено, иначе false
     */
    public function __isset($name)
    {
        return isset($this->_data[$name]);
    }

    /**
     * Сброс свойства
     *
     * @param string $name Имя свойства
     * @return void
     */
    public function __unset($name)
    {
        if (isset($this->_data[$name])) {
            $this->_data[$name] = null;
        }
    }

    public function __toString()
    {
        $string = '';
        foreach ($this->_data as $name => $value) {
        	$string = "$name => $value;";
        }
        return $string;
    }

    public function getManyToMany()
    {
        return $this->_data_many_to_many;
    }

    /**
     * Массив свойств класса
     *
     * @var array
     */
    protected $_data = array();

    /**
     * Массив свойств класса
     *
     * @var array
     */
    protected $_data_many_to_many = array();

    /**
     * Инициализация данных
     *
     * @return Suo_Model_Base_Data
     */
    protected function init()
    {
        return $this;
    }
}