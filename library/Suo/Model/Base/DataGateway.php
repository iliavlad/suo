<?php

class Suo_Model_Base_DataGatewayException extends Suo_Exception
{
    const SUO_GATEWAY_DATA_NAME = 1;

    protected $_messages = array(
        self::SUO_GATEWAY_DATA_NAME => 'Необходимо установить имя данных для класса %s (свойство $_data_name)',
    );
}

abstract class Suo_Model_Base_DataGateway
{
    public function __construct()
    {
        if (empty($this->_data_name)) {
            throw new Suo_Model_Base_DataGatewayException('SUO_GATEWAY_DATA_NAME', get_class($this));
        }
    }

    /**
     * Создание объекта, для доступа к которому используется шлюз
     *
     * @param array|object $data Параметры создаваемого объекта
     * @return Suo_Model_Base_Data
     */
    public function create($data)
    {
        $name = $this->getName();
        $class_name = 'Suo_Model_' . $name;
        $object = new $class_name($data, $this);
        return $object;
    }

    /**
     * Получение всех записей для определенной таблицы
     * Если имя таблицы не указано, берется таблица, к которой относится данный шлюз
     *
     * @param string $name Имя таблицы
     * @return Suo_Model_Base_DataSet
     */
    public function fetchAll($dojo_data = false)
    {
        $name = $this->getName();
        $set_name = 'Suo_Model_' . $name . 'Set';
        $table = $this->getTable($name);
        $records = $table->fetchAll();
        if (false == $dojo_data) {
            $set = new $set_name($records, $this);
        } else {
            $set = new Zend_Dojo_Data('id', $records);
        }
        return $set;
    }

    public function fetchById($id, $name = '')
    {
        $object = null;
/*        if ('' === $name) {
            $name = $this->getName();
            $gateway = $this;
        } else {
            $gateway_name = 'Suo_Model_' . $name . 'Gateway';
            $gateway = new $gateway_name();
        }*/
        $name = $this->getName();
            $gateway = $this;
        $table = $this->getTable($name);
        $rowset = $table->find($id);
        $row = $rowset->current();
        if ($row) {
            $object = $gateway->create($row);
        }
        return $object;
    }

    /**
     * Получение множества объектов по их идентификаторам
     *
     * @param array $ids
     */
    public function fetchSetByIds(array $ids, $name = '')
    {
        if ('' === $name) {
            $name = $this->getName();
            $gateway = $this;
        } else {
            $gateway_name = 'Suo_Model_' . $name . 'Gateway';
            $gateway = new $gateway_name();
        }

        $set_name = 'Suo_Model_' . $name . 'Set';
        if (count($ids) == 0) {
            $records = array();
        } else {
            $table = $this->getTable($name);
            $select = $table->select()
                ->where('`id` IN (?)', $ids);
            $records = $table->fetchAll($select);
        }
        $set = new $set_name($records, $gateway);
        return $set;
    }

    /**
     * Получение таблицы базы данных
     *
     * @param string $table_name Имя таблицы
     * @return Suo_Model_Table_Base
     */
    public function getTable($table_name)
    {
        $table_name = 'Suo_Model_Table_' . ucfirst($table_name);
        if (!isset($this->_tables[$table_name])) {
            $this->_tables[$table_name] = new $table_name();
        }
        return $this->_tables[$table_name];
    }

    public function getName()
    {
        return $this->_data_name;
    }

    public function getObjectUniqueFieldName()
    {
        return $this->_object_unique_field_name;
    }

    public function fetchDependentById($id, $table, $this_name, $dependent_name)
    {
        /**
         * @var Zend_Db_Adapter_Abstract
         */
        $db = Zend_Registry::get('db');
        $query = "SELECT * FROM $table WHERE `$this_name`='$id'";
        $rows = $db->fetchAll($query);
        return $rows;
    }

    public function updateMany($this_id, $many_values, $many_data)
    {
        /**
         * @var Zend_Db_Adapter_Abstract
         */
        $db = Zend_Registry::get('db');
        $query = "DELETE * FROM {$many_data['table']} WHERE `{$many_data['name']}`='$this_id'";
        $db->delete($many_data['table'], "`{$many_data['name']}`='$this_id'");
        if (!empty($many_values)) {
            $first_value = $many_values[0];
            $values_to_insert = array();
            if (is_array($first_value)) { // несколько данные в одной записи вместе с именами
            } else { // простое перечисление значений из формы
                foreach ($many_values as $value) {
                    $values_to_insert[] = "('$this_id', '$value')";
                }
            }
            $sql = "INSERT INTO `{$many_data['table']}`
                        (`{$many_data['name']}`, `{$many_data['dependent_name']}`)
                        VALUES " . implode(',', $values_to_insert);
            $db->query($sql);
        }
    }

    protected $_data_name = '';

    protected $_tables = array();

    protected $_set_of_all = array();

    /**
     * Имя поля, по которому объект отличается от другого
     * Используется при вывода данных в строке адреса в замен id для красоты
     * Например, для устройтв это поле rem_number
     *
     * @var string
     */
    protected $_object_unique_field_name = 'id';
}