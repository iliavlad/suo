<?php
/**
 * Класс для работы с массивом данных
 *
 * @author Ilya Garaga <inform@rem-prim.ru>
 * @since 27.03.2010
 *
 */
abstract class Suo_Model_Base_DataSet implements Iterator, Countable, ArrayAccess
{
    /**
     * Конструктор
     *
     * @param mixed $results Массив данных, по каждому элементу которого можно создать объект, унаследованный от Suo_Model_Base_Data
     * @param Suo_Model_Base_DataGateway $gateway Шлюз для доступа к данным
     */
    public function __construct($results, $gateway)
    {
        $this->setGateway($gateway);
        $this->_recordSet = $results;
/*        $i = 0;
        foreach ($results as $record) {
            $this->_recordIdToRow[$record->id] = $i;
            ++$i;
        }
        if (is_object($results)) {
            $results->rewind();
        } else {
            reset($results);
        }*/
    }

    public function __toString()
    {
        $result = (string)$this->count();
        return $result;
    }

    /**
     * Установка шлюза
     *
     * @param Suo_Model_Base_DataGateway $gateway  Шлюз для доступа к данным
     * @return Suo_Model_Base_DataSet
     */
    public function setGateway(Suo_Model_Base_DataGateway $gateway)
    {
        $this->_gateway = $gateway;
        return $this;
    }

    /**
     * Получение шлюза
     *
     * @return Suo_Model_DeviceGateway
     */
    public function getGateway()
    {
        return $this->_gateway;
    }

    /**
     * Получение количества данных в массиве
     */
    public function count()
    {
        if (null === $this->_count) {
            $this->_count = count($this->_recordSet);
        }
        return $this->_count;
    }

    /**
     * Получение текущего элемента массива
     *
     * @see Iterator#current()
     * @return Suo_Model_Base_Data
     */
    public function current()
    {
        $key = $this->key();
        $record = $this->_recordSet[$key];
        if (!$record instanceof Suo_Model_Base_Data) {
            $record = $this->getGateway()->create($record);
        }
        return $record;
    }

    /**
     * Текущий ключ массива
     *
     * @see Iterator#key()
     * @return mixed | &null, если конец массива или массив пуст
     */
    public function key()
    {
        $key = null;
        if ($this->_recordSet instanceof Iterator) {
            $key = $this->_recordSet->key();
        } else {
            $key = key($this->_recordSet);
        }
        return $key;
    }

    /**
     * Переход к следующему элементу массива
     */
    public function next()
    {
        $next = false;
        if ($this->_recordSet instanceof Iterator) {
            $next = $this->_recordSet->next();
        } else {
            $next = next($this->_recordSet);
        }
        return $next;
    }

    /**
     * Переход к предыдущему элементу массива
     */
    public function rewind()
    {
        $rewind = false;
        if ($this->_recordSet instanceof Iterator) {
            $rewind = $this->_recordSet->rewind();
        } else {
            $rewind = reset($this->_recordSet);
        }
        return $rewind;
    }

    /**
     * Проверка текущего элемента на валидность
     */
    public function valid()
    {
        $valid = false;
        if ($this->_recordSet instanceof Iterator) {
            $valid = $this->_recordSet->valid();
        } else {
        	if (null === $this->key()) {
        	    $valid = null;
        	} else {
                $valid = (bool)$this->current();
        	}
        }
        return $valid;
    }

    /**
     * Do nothing
     *
     * @param int $id
     * @param mixed $value
     */
    public function offsetSet($id, $value)
    {
    }

    /**
     * Do nothing
     *
     * @param int $offset
     * @return bool true, if id is in _recordsSet
     */
    public function offsetExists($id)
    {
        return isset($this->_recordIdToRow[$id]);
    }

    /**
     * Do nothing
     *
     * @param int $offset
     */
    public function offsetUnset($id)
    {
    }

    public function offsetGet($id)
    {
        $result = null;
        if (isset($this->_recordIdToRow[$id])) {
            $result = $this->_recordSet[$this->_recordIdToRow[$id]];
        }
        return $result;
    }

    /**
     * Получение списка id записей
     *
     * @return array
     */
    public function getIds()
    {
        $result = array_keys($this->_recordIdToRow);
        return $result;
    }

    /**
     * Количество элементов в массиве $_recordSet
     *
     * @var integer
     */
    protected $_count;

    /**
     * Шлюз для доступа к данным
     *
     * @var Suo_Model_Base_DataGateway
     */
    protected $_gateway;

    /**
     * Массив с данными для создания данных
     *
     * @var array
     */
    protected $_recordSet;

    protected $_recordIdToRow = array();
}