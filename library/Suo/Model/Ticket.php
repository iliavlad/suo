<?php
/**
 * Заявка
 * Определяет движение клиента в очереди
 *
 * @since 20.03.2010
 * @package SuoModel
 */
class Suo_Model_Ticket extends Suo_Model_Base_ViewData
{
    // TODO Переделать для нормальной работы с базой
    const NEWTICKET     = 1;
    const INQUEUE       = 2;
    const ACCEPTED      = 3;
    const DECLINED      = 4;
    const CLOSED        = 5;
    const WAITCLIENT    = 6;
    const WAITANDRING   = 9;

    /**
     * Обновление статуса заявки
     *
     * @param string $new_status Новый статус заявки
     * @return Suo_Model_Ticket
     */
    public function update($new_status)
    {
        $this->setStatus($new_status);
        if (self::CLOSED == $new_status) {
            $this->queue_id = null;
        }
        $this->save();
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status_id;
    }

    public function __toString()
    {
        $str = $this->id . ' creation_date:' . $this->creation_date . ' operator:' . $this->employee_id;
        return $str;
    }

    public function redirectToRoom($room_id, $operator_id)
    {
        /**
         * @var Suo_Model_TicketGateway
         */
        $gateway = $this->getGateway();
        $date = date('Y-m-d H:i:s');
        $ticket_kit_id = $this->ticket_kit_id;
        if (empty($ticket_kit_id)) {
            // создаём новую группу заявок
            $kit_gateway = new Suo_Model_TicketkitGateway();
            $ticket_kit = $kit_gateway->create(
                    array('description' => 'Check ' . $this->check_id . ' date ' . $date, ));
            $ticket_kit->save();
            $ticket_kit_id = $ticket_kit->id;
            $gateway->addToTicketArchive($operator_id, $this->id, self::NEWTICKET,
                    'Создана группа заявок id ' . $ticket_kit_id);
            $this->ticket_kit_id = $ticket_kit_id;
        }
        // закрываем текущую заявку
        $this->status_id = self::CLOSED;
        $this->employee_id = $operator_id;

        $this->save();
        $gateway->addToTicketArchive($operator_id, $this->id, self::CLOSED, 'Установлен статус');

        /**
         * @var Suo_Model_Ticket
         */
        $new_ticket = $gateway->create(array('creation_date' => $date, 'start_date' => $date, 'room_id' => $room_id,
            'check_id' => $this->check_id, 'status_id' => self::NEWTICKET,
            'ticket_kit_id' => $ticket_kit_id, ));

        $queue_data = $gateway->getQueueIdAndMaxPositionByRoom($room_id);
        // если очередь еще не создана, то создаем ее
        if (null == $queue_data['queue_id']) {
            $queue_gateway = new Suo_Model_QueueGateway();
            $queue = $queue_gateway->create(array('room_id' => $room_id, 'start_date' => $date, ));
            $queue->save();

            $queue_data = array('queue_id' => $queue->id, 'queue_position' => 0, );
        }

        // добавляем данные об очереди в новую заявку
        $new_ticket->queue_id = $queue_data['queue_id'];
        $new_ticket->queue_position = intval($queue_data['queue_position']) + 1;

        $new_ticket->save();

        $gateway->addToTicketArchive($operator_id, $new_ticket->id, self::NEWTICKET, 'Создана заявка');
    }


    /**
     * @param string $new_status
     * @return Suo_Model_Ticket
     */
    protected function setStatus($new_status)
    {
        $this->status_id = $new_status;
        return $this;
    }

    /**
     * @return Suo_Model_Base_Data
     */
    protected function init ()
    {
        return $this;
    }


    public function getDependent()
    {
        return array(
            'employee_id' => array('table' => 'Operator', ),
            'room_id' => array('table' => 'Room', ),
            'client_id' => array('table' => 'Client', ),
            'check_id' => array('table' => 'Check'),
            'queue_id' => array('table' => 'Queue'),
            'status_id' => array('table' => 'Status'),
            'ticket_kit_id' => array('table' => 'Ticketkit'),
        );
    }

    protected $_data = array(
            'id',
            'creation_date',
            'start_date',
            'employee_id',
            'room_id',
            'client_id',
            'check_id',
            'queue_id',
            'queue_position',
            'status_id',
            'priority',
            'ticket_kit_id',
            'window',
        );


}