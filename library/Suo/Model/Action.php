<?php

class Suo_Model_Action extends Suo_Model_Base_ViewData
{
    public function __toString()
    {
        $str = $this->description;
        return $str;
    }


    /**
     * @return Suo_Model_Base_Data
     */
    protected function init ()
    {
       return $this;
    }

    public function getDependent()
    {
        return array(
            'role' => array(
                'table' => 'Role',
                'many' => array('table' => 'role_has_action', 'rule' => 'Action', 'name' => 'action_id',
                                            'dependent_name' => 'role_id', 'value' => null, ),
            ),
        );
    }

    protected $_data = array(
        'id',
        'code',
        'description',
    );

    protected $_data_many_to_many = array(
        'role',
    );


}