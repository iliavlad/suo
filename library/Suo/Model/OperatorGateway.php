<?php

class Suo_Model_OperatorGateway extends Suo_Model_Base_DataGateway
{
    public function getRowByPin($pin)
    {
        $table = $this->getTable($this->getName());
        $select = $table->select()
            ->where('pincode = ?', $pin);
        $row = $table->fetchRow($select);
        return $row;
    }

    public function getRowById($id)
    {
        $table = $this->getTable($this->getName());
        $select = $table->select()
            ->where('id = ?', $id);
        $row = $table->fetchRow($select);
        return $row;
    }

    public function fetchOperatorByRoom($room_id)
    {
        $result = null;
        $table = $this->getTable($this->getName() . 'Room');
        $select = $table->select()
            ->where('room_id = ?', $room_id);
        $row = $table->fetchRow($select);
        if ($row) {
            $result = $this->fetchById($row->employee_id);
        }
        return $result;
    }

    public function fetchOperatorByRoomAndTime($room_id, $time)
    {
        $result = null;
        $table = $this->getTable('Workday');
        $select = $table->select()
            ->where('room_id = ?', $room_id)
            ->where('begin <= ?', $time)
            ->where('end >= ?', $time);
        $row = $table->fetchRow($select);
        if ($row) {
            $result = $this->fetchById($row->employee_id);
        }
        return $result;
    }

    public function fetchAllWithRoomNumber()
    {
        $db = Zend_Registry::get('db');
        $query = "
SELECT IF(TRIM(`room`.`number`) = '', `room`.`description`, `room`.`number`) AS `room_number`,
    `room`.`id` AS `room_id`, `room`.`windowcount`,
    `employee`.`id`, `employee`.`last_name`, `employee`.`first_name`, `employee`.`parent_name`
FROM `employee_has_room`
  JOIN `room`
    ON `room`.`id`=`employee_has_room`.`room_id`
  JOIN `employee`
    ON `employee`.`id`=`employee_has_room`.`employee_id`
  JOIN `employee_has_role`
    ON `employee_has_role`.`employee_id`=`employee_has_room`.`employee_id`
WHERE `employee_has_role`.`role_id`='2'
ORDER BY CAST(`room`.`number` AS UNSIGNED), `employee`.`last_name`, `employee`.`first_name`
        ";

        $result = $db->fetchAll($query);
        return $result;
    }


    public function stopAccept($room_id)
    {
        $db = Zend_Registry::get('db');
        $query = "
UPDATE `room` SET `accept` = '0', `acceptday` = '" . date('Y-m-d') . "' WHERE `room`.`id` = '$room_id'
";

        return $db->query($query);
    }

    protected $_data_name = 'Operator';
}