<?php

class Suo_Model_CheckGateway extends Suo_Model_Base_DataGateway
{
    public function createNewCheckForDay($start_date)
    {
        $code = 1;
        $day = date('Y-m-d', strtotime($start_date));
        $table = $this->getTable($this->getName());
        $select = $table->select()
            ->where('DATE(`start_date`) = ?', $day)
            ->order('code DESC');
        $row = $table->fetchRow($select);
        if ($row) {
            $code = (int)$row->code;
            ++$code;
        }
        $data = array('code' => $code, 'start_date' => $start_date, );
        $check = $this->create($data);
        $check->save();
        return $check;
    }

    public function fetchChecksByRoomIdAndDates($room_id, $date_start, $date_end)
    {
        $db = Zend_Registry::get('db');
        $query = "
SELECT `check`.`code`, `ticket`.`status_id`, `ticket`.`window`
FROM `ticket`
  LEFT JOIN `check`
    ON `check`.`id`=`ticket`.`check_id`
WHERE (`ticket`.`room_id` = '$room_id')
  AND (`ticket`.`start_date` >= '$date_start')
  AND (`ticket`.`start_date` <= '$date_end')
  AND (`ticket`.`status_id`<>'" . Suo_Model_Ticket::CLOSED . "')
  AND (`ticket`.`status_id`<>'" . Suo_Model_Ticket::DECLINED . "')
ORDER BY `ticket`.`queue_position`
";

        $result = $db->fetchAll($query);
        return $result;
    }

    protected $_data_name = 'Check';
}