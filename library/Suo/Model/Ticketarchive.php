<?php

class Suo_Model_Ticketarchive extends Suo_Model_Base_ViewData
{
    public function __toString()
    {
        $str = $this->id . ' ' . $this->description;
        return $str;
    }

    /**
     * @return Suo_Model_Base_Data
     */
    protected function init ()
    {
        return $this;
    }

    protected $_data = array(
            'id',
            'data_date',
            'who_group',
            'who_id',
            'ticket_id',
            'status_id',
            'what',
        );
}