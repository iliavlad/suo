<?php

class Suo_Model_Status extends Suo_Model_Base_ViewData
{
    public function __toString()
    {
        $str = $this->description;
        return $str;
    }

    /**
     * @return Suo_Model_Base_Data
     */
    protected function init ()
    {
        return $this;
    }

    
    protected $_data = array(
        'id',
        'description',
    );
}