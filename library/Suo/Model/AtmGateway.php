<?php

class Suo_Model_AtmGateway extends Suo_Model_Base_DataGateway
{
    public function getAtmByAddr($addr)
    {
        $result = null;
        $table = $this->getTable($this->getName());
        $select = $table->select()
            ->where('netaddress = ?', $addr);
        $row = $table->fetchRow($select);
        if ($row) {
            $result = $this->create($row);
        }
        return $result;
    }

    public function getRoomsByAtmAddr($address)
    {
        $query = "
SELECT `room`.`id`, `room`.`number`, `room`.`description`, `room`.`record`, `room`.`recordcount`, `room`.`max_today_records`, `room`.`accept_pause`, `room`.`accept_pause_day`
FROM `atm_has_room`
LEFT JOIN `room`
    ON `atm_has_room`.`room_id` = `room`.`id`
LEFT JOIN `atm`
    ON `atm_has_room`.`atm_id` = `atm`.`id`
WHERE `atm`.`netaddress`='$address'
ORDER BY CAST(`room`.`number` AS UNSIGNED)
";

        $db = Zend_Registry::get('db');
        $result = $db->fetchAll($query);
        return $result;
    }

    protected $_data_name = 'Atm';
}