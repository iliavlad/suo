<?php
class Suo_Model_Statistic
{
    public function getRoomQueues($sort, $period)
    {
        $db = Zend_Registry::get('db');
        $direction = 'ASC';
        if ('-' == $sort[0]) {
            $sort = mb_substr($sort, -1 * (mb_strlen($sort) - 1));
            $direction = 'DESC';
        }
        if ('description' == $sort) {
            $sort = 'CAST(`room`.`number` AS UNSIGNED)';
        } else if ('queue' == $sort) {

        } else {
            $sort = 'queue';
            $direction = 'DESC';
        }
        switch ($period) {
            case 'today':
                $start = $end = date('Y-m-d');
            break;
            case 'week':
                $start = date('Y-m-d', strtotime('last Monday'));
                $end   = date('Y-m-d');
            break;
            case 'month':
                $start = date('Y-m-01');
                $end   = date('Y-m-d');
            break;
            case 'all':
            default:
                $start = '2010-01-01';
                $end   = date('Y-m-d');
            break;
        }
        $query = "
SELECT `room`.`id`,
        CONCAT(`room`.`number`, ' ', `room`.`description`) as `description`,
        `t`.`queue`
FROM `room`
LEFT JOIN
        (SELECT `ticket`.`room_id`, count(*) AS `queue`
            FROM `ticket`
            WHERE DATE(`ticket`.`start_date`) BETWEEN '$start' AND '$end'
            GROUP BY `ticket`.`room_id`
        ) AS `t`
    ON `t`.`room_id`=`room`.`id`
ORDER BY $sort $direction
        ";

        $result = $db->fetchAll($query);

        return $result;
    }

    public function getOperatorQueues($sort, $period)
    {
        $db = Zend_Registry::get('db');
        $direction = 'ASC';
        if ('-' == $sort[0]) {
            $sort = mb_substr($sort, -1 * (mb_strlen($sort) - 1));
            $direction = 'DESC';
        }
        if ('description' == $sort) {

        } else if ('queue' == $sort) {

        } else {
            $sort = 'queue';
            $direction = 'DESC';
        }
        switch ($period) {
            case 'today':
                $start = $end = date('Y-m-d');
            break;
            case 'week':
                $start = date('Y-m-d', strtotime('last Monday'));
                $end   = date('Y-m-d');
            break;
            case 'month':
                $start = date('Y-m-01');
                $end   = date('Y-m-d');
            break;
            case 'all':
            default:
                $start = '2010-01-01';
                $end   = date('Y-m-d');
            break;
        }
        $query = "
SELECT `employee`.`id`,
        CONCAT(`employee`.`last_name`, ' ', LEFT(`employee`.`first_name`, 1), '.') as `description`,
        `t`.`queue`
FROM `employee`
LEFT JOIN
        (SELECT `ticket`.`employee_id`, count(*) AS `queue`
            FROM `ticket`
            WHERE DATE(`ticket`.`start_date`) BETWEEN '$start' AND '$end'
            GROUP BY `ticket`.`employee_id`
        ) AS `t`
    ON `t`.`employee_id`=`employee`.`id`
JOIN `employee_has_role` ON `employee_has_role`.`employee_id`=`employee`.`id`
WHERE `employee_has_role`.`role_id`='2'
ORDER BY $sort $direction
        ";

        $result = $db->fetchAll($query);

        return $result;
    }


    public function getTicketsToDecline()
    {
        $db = Zend_Registry::get('db');
        $date = date('Y-m-d');
        $query = "
SELECT `ticket`.`id`, CONCAT(`room`.`number`, ' ', `room`.`description`) as `room_description`, `ticket`.`start_date`
FROM `ticket`
LEFT JOIN `room`
        ON `room`.`id`=`ticket`.`room_id`
WHERE DATE(`ticket`.`start_date`)='$date' AND
     `ticket`.`status_id`<>' " . Suo_Model_Ticket::CLOSED . "' AND
     `ticket`.`status_id`<>'" . Suo_Model_Ticket::DECLINED . "'
ORDER BY CAST(`room`.`number` AS UNSIGNED) ASC, `ticket`.`start_date` ASC
        ";

        $result = $db->fetchAll($query);

        return $result;
    }


    public function fetchTicketsGroupedRoomAndDate($date_start, $date_end)
    {
        $db = Zend_Registry::get('db');
        $query = "
SELECT `ticket`.`room_id` AS `id`,
        CONCAT(`room`.`number`, ' ', `room`.`description`) AS `room`,
        DATE(`ticket`.`start_date`) AS `day`,
        COUNT(*) AS `count`
FROM `ticket`
LEFT JOIN `room` ON `ticket`.`room_id` = `room`.`id`
WHERE `ticket`.`start_date` BETWEEN '$date_start' AND '$date_end'
GROUP BY `ticket`.`room_id`, `day`
ORDER BY CAST(`room`.`number` AS UNSIGNED), `day`
";
        $result = $db->fetchAll($query);

        return $result;
    }
}