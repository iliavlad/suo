<?php

class Suo_Model_Atm extends Suo_Model_Base_ViewData
{
    public function addTicket($room_id, $start_date)
    {
        // получаем данные для заявки
        $ticket_data = array();
        // кабинет
        $ticket_data['room_id'] = $room_id;
        $room_gateway = new Suo_Model_RoomGateway();
        $room = $room_gateway->fetchById($room_id);
        $room_number = '';
        if (!empty($room->number)) {
            $room_number = 'Кабинет №' . $room->number;
        }
        $room_description = $room->description;
        // оператор
        $operator_gateway = new Suo_Model_OperatorGateway();
        $date = date('Y-m-d H:i:s');
        $emp_caption = '';
        $employee = $operator_gateway->fetchOperatorByRoomAndTime($room_id, $date);
        $employee_id = null;
        if ($employee) {
            $employee_id = $employee->id;
            $ticket_data['employee_id'] = $employee->id;
            $emp_caption = $employee->__toString();
        }
        // дата создания
        $ticket_data['creation_date'] = $date;
        // дата начала
        $ticket_data['start_date'] = $start_date;
        // статус
        $status = 1;
        $ticket_data['status_id'] = $status;
        // очередь
        $ticket_gateway = new Suo_Model_TicketGateway();
        $queue_gateway = new Suo_Model_QueueGateway();
        $queue = $queue_gateway->fetchQueueByRoom($room_id, $start_date);
        $queue_position = 1;
        if ($queue) {
            $queue_id = $queue->id;
            $queue_position = $ticket_gateway->fetchMaxPositionInQueue($queue_id) + 1;
        } else {
            $queue = $queue_gateway->create(array('room_id' => $room_id, 'start_date' => $date, 'employee_id' => $employee_id, ));
            $queue->save();
            $queue_id = $queue->id;
        }
        $ticket_data['queue_id'] = $queue_id;
        $ticket_data['queue_position'] = $queue_position;
        // талон
        $check_gateway = new Suo_Model_CheckGateway();
        $check = $check_gateway->createNewCheckForDay($start_date);
        $ticket_data['check_id'] = $check->id;
        $ticket = $ticket_gateway->create($ticket_data);
        $ticket->save();
        $pos = $ticket_gateway->getOpenTicketCountByRoomAndDate($room_id, date('Y-m-d', strtotime($start_date)));
        $result = array(
            'check_title' => 'Поликлиника',
            'check_number' => $check->code,
            'check_operator' => $emp_caption,
            'check_room_number' => $room_number,
            'check_room_description' => $room_description,
            'check_start_date' => date('d.m.Y', strtotime($start_date)),
            'check_position' => 'Вы ' . $pos . 'й(я) в очереди',
        );

     $logger = Zend_Log::factory(array(
        'timestampFormat' => 'y-m-d H:i:s',
        array(
            'writerName'   => 'Stream',
            'writerParams' => array(
                'stream'   => '/tmp/suo.log',
            ),
            'formatterName' => 'Simple',
            'formatterParams' => array(
                'format'   => '%timestamp%: %message%',
            )
        )
    ));

    $logger->log(implode(',', $result) . "\n", Zend_Log::INFO);

        return $result;
    }


    public function __toString()
    {
        $str = $this->netaddress . ' ' . $this->place;
        return $str;
    }

    /**
     * @return Suo_Model_Base_Data
     */
    protected function init ()
    {
        return $this;
    }

    public function getDependent()
    {
        return array(
            'room' => array(
                'table' => 'Room',
                'many' => array('table' => 'atm_has_room', 'rule' => 'Atm', 'name' => 'atm_id',
                                            'dependent_name' => 'room_id', 'value' => null, ),
                      ),
        );
    }

    protected $_data = array(
        'id',
        'netaddress',
        'place',
    );

    protected $_data_many_to_many = array(
        'room',
    );

}
