<?php

class Suo_Model_PanelGateway extends Suo_Model_Base_DataGateway
{
    public function getPanelByAddr($addr)
    {
        $result = null;
        $table = $this->getTable($this->getName());
        $select = $table->select()
            ->where('netaddress = ?', $addr);
        $row = $table->fetchRow($select);
        if ($row) {
            $result = $this->create($row);
        }
        return $result;
    }

    protected $_data_name = 'Panel';
}