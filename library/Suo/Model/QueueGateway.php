<?php

class Suo_Model_QueueGateway extends Suo_Model_Base_DataGateway
{
    public function fetchQueueByRoom($room_id, $start_date)
    {
        $result = null;
        $table = $this->getTable($this->getName());
        $select = $table->select()
            ->where('room_id = ?', $room_id)
            ->where('DATE(start_date) = ?', date('Y-m-d', strtotime($start_date)));
        $row = $table->fetchRow($select);
        if ($row) {
            $result = $this->create($row);
        }
        return $result;
    }

    public function fetchQueueByOperator($operator_id)
    {
        $result = null;
        $table = $this->getTable($this->getName());
        $select = $table->select()
            ->where('employee_id = ?', $operator_id)
            ->where('DATE(`start_date`) = ?', date('Y-m-d'));
        $row = $table->fetchRow($select);
        if ($row) {
            $result = $this->create($row);
        }
        return $result;
    }

    protected $_data_name = 'Queue';
}