<?php

class Suo_Model_Check extends Suo_Model_Base_ViewData
{
    public function __toString()
    {
        $str = $this->id . ' ' . $this->code;
        return $str;
    }

    /**
     * @return Suo_Model_Base_Data
     */
    protected function init ()
    {
        return $this;
    }

    protected $_data = array(
        'id',
        'code',
        'start_date',
    );
}