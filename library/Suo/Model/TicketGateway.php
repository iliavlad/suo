<?php

class Suo_Model_TicketGateway extends Suo_Model_Base_DataGateway
{
    public function fetchTicketsByQueueId($queue_id)
    {
        $table = $this->getTable($this->getName());
        $select = $table->select()
            ->where('queue_id = ?', $queue_id)
            ->order('queue_position');
        $records = $table->fetchAll($select);
        $set = new Suo_Model_TicketSet($records, $this);
        return $set;
    }

    public function fetchChecksByQueueId($queue_id)
    {
        $tickets = $this->fetchTicketsByQueueId($queue_id);
        $checks_id = array();
        foreach ($tickets as $ticket) {
            $checks_id[] = $ticket->check_id;
        }
        $check_gateway = new Suo_Model_CheckGateway();
        $checks = $check_gateway->fetchSetByIds($checks_id);
        return $checks;
    }

    /**
     * @param int $queue_id
     * @return int
     */
    public function fetchMaxPositionInQueue($queue_id)
    {
        $result = 0;
        $table = $this->getTable($this->getName());
        $select = $table->select()
            ->from($table, array('MAX(`queue_position`) AS queue_position', ))
            ->where('queue_id = ?', $queue_id);
        $row = $table->fetchRow($select);
        if ($row['queue_position']) {
            $result = $row['queue_position'];
        }
        return $result;
    }

    public function fetchTicketsAndChecksNumbersByOperator($operator_id, $room_id)
    {
        $result = array();

        $db = Zend_Registry::get('db');
        $date = date('Y-m-d');
        $query = "
SELECT `ticket`.`id`, `check`.`code`
FROM `ticket`
LEFT JOIN `check` ON
    `ticket`.`check_id` = `check`.`id`
WHERE DATE(`ticket`.`start_date`)='$date' AND
     `ticket`.`status_id`<>' " . Suo_Model_Ticket::CLOSED . "' AND
     `ticket`.`status_id`<>'" . Suo_Model_Ticket::DECLINED . "' AND
    (`ticket`.`employee_id` = '$operator_id' OR
        (`ticket`.`employee_id` IS NULL AND
            `ticket`.`room_id` = '$room_id')
    )
ORDER BY `ticket`.`queue_position`
"; // `ticket`.`status_id` DESC,  сортировка по статусу, чтобы первой шла заявка со статусом "Ждем клиента" или "Вызван"

        $result = $db->fetchAll($query);

        return $result;
    }

    public function setTicketStatus($operator_id, $ticket_id, $status_id, $room_id = null)
    {
        $db = Zend_Registry::get('db');
        $operator_add = ' ';
        if (Suo_Model_Ticket::CLOSED == $status_id) {
            $operator_add = ", `employee_id`='$operator_id' ";
        }
        $query = "
UPDATE `ticket`
SET `status_id`='$status_id'
        $operator_add
WHERE `id`='$ticket_id'
";
        $db->query($query);

        $query = "
INSERT INTO `ticket_archive`
(`data_date`, `who_group`, `who_id`, `ticket_id`, `status_id`, `what`)
VALUES ('" . date('Y-m-d H:i:s') . "', 'operator', '$operator_id', '$ticket_id', '$status_id',
        'Установлен статус')
";

        $db->query($query);
    }

    public function setManyTicketsStatusByPanel($panel_id, $a_ticket_id, $status_id, $room_id = null)
    {
        $db = Zend_Registry::get('db');
        $operator_add = ' ';
        if (Suo_Model_Ticket::CLOSED == $status_id) {
            $operator_add = ", `employee_id`='$operator_id' ";
        }
        $s_ids = "'" . implode("', '", $a_ticket_id) . "'";
        $query = "
UPDATE `ticket`
SET `status_id`='$status_id'
        $operator_add
WHERE `id` IN ($s_ids)
";
        $db->query($query);

        $s_date = date('Y-m-d H:i:s');
        $a_values = array();
        foreach ($a_ticket_id as $s_id) {
            $a_values[] = "('$s_date', 'panel', '$panel_id', '$s_id', '$status_id', 'Установлен статус')";
        }
        $s_values = implode(',', $a_values);

        $query = "
INSERT INTO `ticket_archive`
(`data_date`, `who_group`, `who_id`, `ticket_id`, `status_id`, `what`)
VALUES $s_values
";

        $db->query($query);
    }

    public function updateTicketPosition($operator_id, $ticket_id, $add_position)
    {
        $db = Zend_Registry::get('db');
        $query = "
SELECT `ticket`.`queue_id`, `ticket`.`queue_position`
FROM `ticket`
WHERE `ticket`.`id`='$ticket_id'
";
        $result = $db->fetchRow($query);
        
        $query = "
UPDATE `ticket`
SET `queue_position`=(`queue_position`+1)
WHERE `queue_id`='{$result['queue_id']}' AND
        `queue_position`>='" . ($result['queue_position'] + $add_position) . "'
ORDER BY `queue_position`
";

        $db->query($query);

        $query = "
UPDATE `ticket`
SET `queue_position`=(`queue_position`+$add_position),
    `status_id`='" . Suo_Model_Ticket::NEWTICKET . "'
WHERE `ticket`.`id`='$ticket_id'";

        $db->query($query);

        $query = "
INSERT INTO `ticket_archive`
(`data_date`, `who_group`, `who_id`, `ticket_id`, `status_id`, `what`)
VALUES ('" . date('Y-m-d H:i:s') . "', 'operator', '$operator_id', '$ticket_id', '" . Suo_Model_Ticket::NEWTICKET . "',
        'Установлен статус')
";

        $db->query($query);
            /*
            $q = "UPDATE `ticket` SET `queue_position`=(`queue_position`+1) WHERE `queue_id`='{$this->_current_queue_id}' AND " .
                "`queue_position`>=" . ($this->_current_queue_position + $this->_next_plus_position);
            $res = mysql_query($q);
            // обновляем позицию текущей заявки и делаем ее новой
            $status_id = self::NEWTICKET;
            $position = $this->_current_queue_position + $this->_next_plus_position;
			$emp_add = '';
			if (true == $this->_employee_id_was_null) {
				$emp_add = '`employee_id`=NULL, ';
			}
            $q = "UPDATE `ticket` SET `status_id`='$status_id', $emp_add `queue_position`='$position' WHERE `id`='{$this->_current_ticket}'";
            $res = mysql_query($q);
            $q = "INSERT INTO `ticket_archive` (`data_date`, `who_group`, `who_id`, `ticket_id`, `status_id`, `what`) " .
                "VALUES ('" . date('Y-m-d H:i:s') . "', 'operator', '" . $this->_user_id . "', '" . $this->_current_ticket . "', " .
                    "'$status_id', 'Установлен статус')";
            $res = mysql_query($q);
  */

        }

    public function addToTicketArchive($operator_id, $ticket_id, $status_id, $what)
    {
        $db = Zend_Registry::get('db');

        $query = "
INSERT INTO `ticket_archive`
(`data_date`, `who_group`, `who_id`, `ticket_id`, `status_id`, `what`)
VALUES ('" . date('Y-m-d H:i:s') . "', 'operator', '$operator_id', '$ticket_id', '$status_id', '$what')
";

        $db->query($query);
    }

    public function getQueueIdAndMaxPositionByRoom($room_id)
    {
        $db = Zend_Registry::get('db');

        $query = "
SELECT `ticket`.`queue_id`, MAX(`ticket`.`queue_position`) as `queue_position`
FROM `ticket`
WHERE `ticket`.`room_id`='$room_id' AND
        DATE(`ticket`.`start_date`)='" . date('Y-m-d') . "'
";

        $result = $db->fetchRow($query);

        return $result;
    }

    public function declineTickets($tickets)
    {
        $db = Zend_Registry::get('db');
        $status_id = Suo_Model_Ticket::DECLINED;
        $query = "
UPDATE `ticket`
SET `status_id`='$status_id'
WHERE `ticket`.`id` IN (' " . implode("', '", $tickets). "')";

        $db->query($query);

        $values = array();
        $date = date('Y-m-d H:i:s');
        foreach ($tickets as $ticket_id) {
            $values[] = "('$date', 'operator', '1', '$ticket_id', '$status_id', 'Отмена заявки администратором')";
        }

        $query = "
INSERT INTO `ticket_archive`
(`data_date`, `who_group`, `who_id`, `ticket_id`, `status_id`, `what`)
VALUES " . implode(', ', $values) . "
";

        $db->query($query);
    }

    public function fetchNumberOfTicketsByRoomAndDate($room_id, $date_start, $date_end)
    {
        $db = Zend_Registry::get('db');
        $query = "
SELECT DATE(`ticket`.`start_date`) AS `day`, COUNT(*) AS `count`
FROM `ticket`
WHERE `ticket`.`room_id`='$room_id' AND
      `ticket`.`start_date` >= '$date_start' AND
      `ticket`.`start_date` <= '$date_end' AND
      `ticket`.`status_id`<>'" . Suo_Model_Ticket::CLOSED . "' AND
      `ticket`.`status_id`<>'" . Suo_Model_Ticket::DECLINED . "'
GROUP BY `day`
";
        $result = $db->fetchAll($query);

        return $result;
    }

    public function getOpenTicketCountByRoomAndDate($room_id, $date)
    {
        $db = Zend_Registry::get('db');
        $query = "
SELECT COUNT(*) AS `count`
FROM `ticket`
WHERE `ticket`.`room_id`='$room_id' AND
      DATE(`ticket`.`start_date`) = '$date' AND
     `ticket`.`status_id`<>'" . Suo_Model_Ticket::CLOSED . "' AND
     `ticket`.`status_id`<>'" . Suo_Model_Ticket::DECLINED . "'
LIMIT 1
";
        $row = $db->fetchRow($query);
        $result = $row['count'];

        return $result;
    }

    public function fetchTodayNumberOfTicketsByRoom($rooms_id)
    {
        $db = Zend_Registry::get('db');
        $query = "
SELECT `ticket`.`room_id`, COUNT(*) AS `count`, `room`.`max_today_records`
FROM `ticket`
LEFT JOIN `room`
    ON `ticket`.`room_id` = `room`.`id`
WHERE `ticket`.`room_id` IN ('" . implode("', '", $rooms_id) . "') AND
      DATE(`ticket`.`start_date`) = CURDATE()
GROUP BY `ticket`.`room_id`
";

        $result = $db->fetchAll($query);

        return $result;
    }

    protected $_data_name = 'Ticket';
}