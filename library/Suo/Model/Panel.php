<?php

class Suo_Model_Panel extends Suo_Model_Base_ViewData
{
    public function __toString()
    {
        $str = $this->netaddress . ' ' . $this->place;
        return $str;
    }

    /**
     * @return Suo_Model_Base_Data
     */
    protected function init ()
    {
        return $this;
    }

    public function getDependent()
    {
        return array(
            'room' => array(
                'table' => 'Room',
                'many' => array('table' => 'panel_has_room', 'rule' => 'Panel', 'name' => 'panel_id',
                            'dependent_name' => 'room_id', 'value' => null, ),
            ),
        );
    }


    protected $_data = array(
        'id',
        'netaddress',
        'place',
    );

    protected $_data_many_to_many = array(
        'room',
    );

}