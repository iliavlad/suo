<?php

class Suo_Model_Queue extends Suo_Model_Base_ViewData
{
    public function __toString()
    {
        $str = $this->id . ' ' . $this->employee_id . ' ' . $this->room_id;
        return $str;
    }

    /**
     * @return Suo_Model_Base_Data
     */
    protected function init ()
    {
        return $this;
    }

    public function getDependent()
    {
        return array(
            'employee_id' => array('table' => 'Operator', ),
            'room_id' => array('table' => 'Room', ),
        );
    }
    protected $_data = array(
        'id',
        'employee_id',
        'room_id',
        'start_date',
    );

}