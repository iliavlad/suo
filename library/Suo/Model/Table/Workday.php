<?php

class Suo_Model_Table_Workday extends Suo_Model_Table_Base
{
    protected $_name = 'workday';

    protected $_referenceMap = array(
        'Operator' => array(
            'columns' => 'employee_id',
            'refTableClass' => 'Suo_Model_Table_Operator',
            'refColumns' => 'id',
        ),
        'Room' => array(
            'columns' => 'room_id',
            'refTableClass' => 'Suo_Model_Table_Room',
            'refColumns' => 'id',
        ),
    );
}