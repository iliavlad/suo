<?php

class Suo_Model_Table_Action extends Suo_Model_Table_Base
{
    protected $_name = 'action';
    protected $_cols = array('id', 'code', 'description', );
    //protected $_primary = array('id', );

    protected $_dependentTables = array('Suo_Model_Table_RoleAction', );
}