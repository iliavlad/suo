<?php

class Suo_Model_Table_Client extends Suo_Model_Table_Base
{
    protected $_name = 'client';
    protected $_cols = array('id', 'code', 'last_name', 'first_name', 'parent_name', 'priority', );
    //protected $_primary = array('id', );

    protected $_dependentTables = array('Suo_Model_Table_Ticket', );
}