<?php

class Suo_Model_Table_Queue extends Suo_Model_Table_Base
{
    protected $_name = 'queue';
    protected $_cols = array('id', 'employee_id', 'room_id', 'start_date', );
    //protected $_primary = array('id', );

    protected $_referenceMap = array(
        'Operator' => array(
            'columns' => 'employee_id',
            'refTableClass' => 'Suo_Model_Table_Operator',
            'refColumns' => 'id',
        ),
        'Room' => array(
            'columns' => 'room_id',
            'refTableClass' => 'Suo_Model_Table_Room',
            'refColumns' => 'id',
        ),
    );
}