<?php

class Suo_Model_Table_Ticketkit extends Suo_Model_Table_Base
{
    protected $_name = 'ticket_kit';
    protected $_cols = array('id', 'description', );
    //protected $_primary = array('id', );

    protected $_dependentTables = array('Suo_Model_Table_Ticket', );
}