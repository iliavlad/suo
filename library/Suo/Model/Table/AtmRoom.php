<?php

class Suo_Model_Table_AtmRoom extends Suo_Model_Table_Base
{
    protected $_name = 'atm_has_room';
    protected $_cols = array('atm_id', 'room_id');
    //protected $_primary = array('atm_id', 'room_id', );

    protected $_referenceMap = array(
        'Atm' => array(
            'columns' => 'atm_id',
            'refTableClass' => 'Suo_Model_Table_Atm',
            'refColumns' => 'id',
        ),
        'Room' => array(
            'columns' => 'room_id',
            'refTableClass' => 'Suo_Model_Table_Room',
            'refColumns' => 'id',
        ),
    );
}