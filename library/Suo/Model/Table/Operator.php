<?php

class Suo_Model_Table_Operator extends Suo_Model_Table_Base
{
    protected $_name = 'employee';
    protected $_cols = array('id', 'last_name', 'first_name', 'parent_name', 'pincode', );
    //protected $_primary = array('id', );

    protected $_dependentTables = array('Suo_Model_Table_OperatorRoom', 'Suo_Model_Table_Queue', 'Suo_Model_Table_OperatorRole',
        'Suo_Model_Table_Workday', );
}