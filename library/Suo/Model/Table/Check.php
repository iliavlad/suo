<?php

class Suo_Model_Table_Check extends Suo_Model_Table_Base
{
    protected $_name = 'check';
    protected $_cols = array('id', 'code', 'start_date', );
    //protected $_primary = array('id', );

    protected $_dependentTables = array('Suo_Model_Table_Ticket', );
}