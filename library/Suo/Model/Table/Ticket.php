<?php

class Suo_Model_Table_Ticket extends Suo_Model_Table_Base
{
    protected $_name = 'ticket';

    protected $_referenceMap = array(
        'Operator' => array(
            'columns' => 'employee_id',
            'refTableClass' => 'Suo_Model_Table_Operator',
            'refColumns' => 'id',
        ),
        'Room' => array(
            'columns' => 'room_id',
            'refTableClass' => 'Suo_Model_Table_Room',
            'refColumns' => 'id',
        ),
        'Client' => array(
            'columns' => 'client_id',
            'refTableClass' => 'Suo_Model_Table_Client',
            'refColumns' => 'id',
        ),
        'Check' => array(
            'columns' => 'check_id',
            'refTableClass' => 'Suo_Model_Table_Check',
            'refColumns' => 'id',
        ),
        'Queue' => array(
            'columns' => 'queue_id',
            'refTableClass' => 'Suo_Model_Table_Queue',
            'refColumns' => 'id',
        ),
        'Ticketkit' => array(
            'columns' => 'ticket_kit_id',
            'refTableClass' => 'Suo_Model_Table_Ticketkit',
            'refColumns' => 'id',
        ),
    );
}