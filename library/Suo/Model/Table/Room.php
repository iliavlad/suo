<?php

class Suo_Model_Table_Room extends Suo_Model_Table_Base
{
    protected $_name = 'room';
    protected $_cols = array('id', 'description', );
    //protected $_primary = array('id', );

    protected $_dependentTables = array('Suo_Model_Table_AtmRoom', 'Suo_Model_Table_PanelRoom', 'Suo_Model_Table_OperatorRoom',
        'Suo_Model_Table_Queue', 'Suo_Model_Table_Workday', );
}