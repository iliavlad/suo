<?php

class Suo_Model_Table_OperatorRole extends Suo_Model_Table_Base
{
    protected $_name = 'employee_has_role';
    protected $_cols = array('employee_id', 'role_id');
    //protected $_primary = array('employee_id', 'role_id', );

    protected $_referenceMap = array(
        'Operator' => array(
            'columns' => 'employee_id',
            'refTableClass' => 'Suo_Model_Table_Operator',
            'refColumns' => 'id',
        ),
        'Role' => array(
            'columns' => 'role_id',
            'refTableClass' => 'Suo_Model_Table_Role',
            'refColumns' => 'id',
        ),
    );
}