<?php

class Suo_Model_Table_RoleAction extends Suo_Model_Table_Base
{
    protected $_name = 'role_has_action';
    protected $_cols = array('role_id', 'action_id');
    //protected $_primary = array('role_id', 'action_id', );

    protected $_referenceMap = array(
        'Role' => array(
            'columns' => 'role_id',
            'refTableClass' => 'Suo_Model_Table_Role',
            'refColumns' => 'id',
        ),
        'Action' => array(
            'columns' => 'action_id',
            'refTableClass' => 'Suo_Model_Table_Action',
            'refColumns' => 'id',
        ),
    );
}