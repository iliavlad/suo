<?php

class Suo_Model_Table_Atm extends Suo_Model_Table_Base
{
    protected $_name = 'atm';
    protected $_cols = array('id', 'netaddress', 'place', );
    //protected $_primary = array('id', );

    protected $_dependentTables = array('Suo_Model_Table_AtmRoom', );
}