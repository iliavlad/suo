<?php

class Suo_Model_Table_Status extends Suo_Model_Table_Base
{
    protected $_name = 'status';
    protected $_cols = array('id', 'description', );
    //protected $_primary = array('id', );

    protected $_dependentTables = array('Suo_Model_Table_Ticket', );
}