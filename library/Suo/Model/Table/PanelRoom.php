<?php

class Suo_Model_Table_PanelRoom extends Suo_Model_Table_Base
{
    protected $_name = 'panel_has_room';
    protected $_cols = array('panel_id', 'room_id');
    //protected $_primary = array('panel_id', 'room_id', );

    protected $_referenceMap = array(
        'Panel' => array(
            'columns' => 'panel_id',
            'refTableClass' => 'Suo_Model_Table_Panel',
            'refColumns' => 'id',
        ),
        'Room' => array(
            'columns' => 'room_id',
            'refTableClass' => 'Suo_Model_Table_Room',
            'refColumns' => 'id',
        ),
    );
}