<?php

class Suo_Model_Table_Role extends Suo_Model_Table_Base
{
    protected $_name = 'role';
    protected $_cols = array('id', 'description', );
    //protected $_primary = array('id', );

    protected $_dependentTables = array('Suo_Model_Table_OperatorRole', 'Suo_Model_Table_RoleAction', );
}