<?php

class Suo_Model_Table_OperatorRoom extends Suo_Model_Table_Base
{
    protected $_name = 'employee_has_room';
    protected $_cols = array('employee_id', 'room_id');
    //protected $_primary = array('employee_id', 'room_id', );

    protected $_referenceMap = array(
        'Operator' => array(
            'columns' => 'employee_id',
            'refTableClass' => 'Suo_Model_Table_Operator',
            'refColumns' => 'id',
        ),
        'Room' => array(
            'columns' => 'room_id',
            'refTableClass' => 'Suo_Model_Table_Room',
            'refColumns' => 'id',
        ),
    );
}