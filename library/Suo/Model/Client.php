<?php

class Suo_Model_Client extends Suo_Model_Base_ViewData
{
    public function __toString()
    {
        $str = $this->code . ' ' .
            $this->last_name . ' ' . iconv_substr($this->first_name, 0, 1) . '. '. iconv_substr($this->parent_name, 0, 1) . '.';
        return $str;
    }

    /**
     * @return Suo_Model_Base_Data
     */
    protected function init ()
    {
        return $this;
    }

    protected $_data = array(
        'id',
        'code',
        'last_name',
        'first_name',
        'parent_name',
        'priority',
    );
}