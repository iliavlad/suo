<?php

class Suo_Model_TicketarchiveGateway extends Suo_Model_Base_DataGateway
{
    protected $_data_name = 'Ticketarchive';

    public function addByAtm($atm_id, $ticket_id, $ticket_status)
    {
        $data = array('data_date' => date('Y-m-d H:i:s'), 'who_group' => 'atm', 'who_id' => $atm_id,
            'ticket_id' => $ticket_id, 'status_id' => $ticket_status, 'what' => 'Добавлена заявка', );
        $record = new Suo_Model_Ticketarchive($data, $this);
        $record->save();
    }
}