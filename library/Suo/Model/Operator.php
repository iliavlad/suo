<?php
/**
 * Оператор
 * Производит обслуживание заявок
 *
 * @since 20.03.2010
 * @package SuoModel
 */
class Suo_Model_Operator extends Suo_Model_Base_ViewData implements Zend_Auth_Adapter_Interface
{
    /**
     * Добавить заявку
     * Оператор добавляет заявку, чтобы отправить клиента к другому оператору
     *
     * @return Suo_Model_Ticket
     */
    public function addTicket()
    {
        $ticket = new Suo_Model_Ticket();
        return $ticket;
    }

    /**
     * Принять заявку
     * Оператор начинает работу с пришедшим клиентом
     *
     * @param Suo_Model_Ticket $ticket Заявка
     */
    public function acceptTicket(Suo_Model_Ticket $ticket)
    {
        $ticket->update(Suo_Model_Ticket::ACCEPTED);
    }

    /**
     * Отменить заявку
     * Оператор не может обслужить заявку, заявка уйдет из очереди
     *
     * @param Suo_Model_Ticket $ticket Заявка
     */
    public function declineTicket(Suo_Model_Ticket $ticket)
    {
        $ticket->update(Suo_Model_Ticket::DECLINED);
    }

    /**
     * Закрыть заявку
     * Оператор завершил работу с клиентом
     *
     * @param Suo_Model_Ticket $ticket Заявка
     */
    public function closeTicket(Suo_Model_Ticket $ticket)
    {
        $ticket->update(Suo_Model_Ticket::CLOSED);
    }

    public function __toString()
    {
        $str = $this->last_name . ' ' . iconv_substr($this->first_name, 0, 1) . '. '. iconv_substr($this->parent_name, 0, 1) . '.';
        return $str;
    }

    /* (non-PHPdoc)
     * @see Zend/Auth/Adapter/Zend_Auth_Adapter_Interface#authenticate()
     */
    public function authenticate()
    {
        $gateway = $this->getGateway();
        $row = $gateway->getRowById($this->id);
        if (null !== $row && $row['pincode'] === $this->pincode) {
            // passed
            $this->populate($row->toArray());
            $result = new Zend_Auth_Result(Zend_Auth_Result::SUCCESS, $this);
        } else {
            $result = new Zend_Auth_Result(Zend_Auth_Result::FAILURE_UNCATEGORIZED, null);
        }

        return $result;
    }

    /**
     * @return Suo_Model_Base_Data
     */
    protected function init ()
    {
        return $this;
    }

    public function getDependent()
    {
        return array(
            'role' => array(
                'table' => 'Role',
                'many' => array('table' => 'employee_has_role', 'rule' => 'Employee', 'name' => 'employee_id',
                                            'dependent_name' => 'role_id', 'value' => null, ),
            ),
            'room' => array(
                'table' => 'Room',
                'many' => array('table' => 'employee_has_room', 'rule' => 'Employee', 'name' => 'employee_id',
                            'dependent_name' => 'room_id', 'value' => null, ),
            ),
        );
    }

    protected  $_data = array(
        'id',
        'last_name',
        'first_name',
        'parent_name',
        'pincode',
    );

    protected $_data_many_to_many = array(
        'role',
        'room',
    );

}