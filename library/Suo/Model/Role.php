<?php

class Suo_Model_Role extends Suo_Model_Base_ViewData
{
    public function __toString()
    {
        $str = $this->id . ' ' . $this->description;
        return $str;
    }

    /**
     * @return Suo_Model_Base_Data
     */
    protected function init ()
    {
        return $this;
    }

    public function getDependent()
    {
        return array(
            'action' => array(
                'table' => 'Action',
                'many' => array('table' => 'role_has_action', 'rule' => 'Role', 'name' => 'role_id',
                                            'dependent_name' => 'action_id', 'value' => null, ),
            ),
            'operator' => array(
                'table' => 'Operator',
                'many' => array('table' => 'employee_has_role', 'rule' => 'Role', 'name' => 'role_id',
                                            'dependent_name' => 'employee_id', 'value' => null, ),
            ),
        );
    }

    protected $_data = array(
        'id',
        'description',
    );

    protected $_data_many_to_many = array(
        'action',
        'operator',
    );
}