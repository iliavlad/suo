<?php

class Suo_Model_Room extends Suo_Model_Base_ViewData
{
    public function __toString()
    {
        $str = $this->number . ' ' . $this->description;
        return $str;
    }

    /**
     * Установка значения свойства
     *
     * @param string $name Имя свойства
     * @param mixed $value Значение свойства
     * @return void
     */
    public function __set($name, $value)
    {
        if ('room_number' !== $name) {
            parent::__set($name, $value);
        }
    }

    /**
     * @return Suo_Model_Base_Data
     */
    protected function init ()
    {
        return $this;
    }

    protected function beforeSave(Zend_Db_Table_Row_Abstract $row)
    {
        if (empty($row->acceptday)) {
            $row->acceptday = '2010-01-01';
        }
    }

    protected function beforeInsert(array &$data)
    {
        if (empty($data['acceptday'])) {
            $data['acceptday'] = '2010-01-01';
        }
    }

    public function getDependent()
    {
        return array(
            'operator' => array(
                'table' => 'Operator',
                'many' => array('table' => 'employee_has_room', 'rule' => 'Room', 'name' => 'room_id',
                                            'dependent_name' => 'employee_id', 'value' => null, ),
            ),
            'atm' => array(
                'table' => 'Atm',
                'many' => array('table' => 'atm_has_room', 'rule' => 'Room', 'name' => 'room_id',
                                            'dependent_name' => 'atm_id', 'value' => null, ),
            ),
            'panel' => array(
                'table' => 'Panel',
                'many' => array('table' => 'panel_has_room', 'rule' => 'Room', 'name' => 'room_id',
                            'dependent_name' => 'panel_id', 'value' => null, ),
            ),
        );
    }

    protected $_data = array(
        'id',
        'number',
        'description',
        'accept',
        'acceptday',
		'accept_pause',
		'accept_pause_day',
        'record',
        'recordcount',
        'windowcount',
		'max_today_records'
    );

    protected $_data_many_to_many = array(
        'operator',
        'atm',
        'panel',
    );
}