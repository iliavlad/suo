<?php

class Suo_View_Helper_Base
{
    public function setView(Zend_View $view)
    {
        $this->view = $view;
    }

    /**
     * @var Zend_View
     */
    public $view;
}