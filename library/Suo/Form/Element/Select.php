<?php

class Suo_Form_Element_Select extends Zend_Form_Element_Select
{
    public function init()
    {
        $this->setDisableLoadDefaultDecorators(true);
        $this->addDecorator('ViewHelper')
            ->addDecorator('Errors')
            ->addDecorator('Description', array('tag' => 'p', 'class' => 'description'))
//            ->addDecorator('HtmlTag', array('tag' => 'dd',
//                                            'id'  => $this->getName() . '-element'))
            ->addDecorator('Label');//, array('tag' => 'dt'));
    }
}