<?php

class Suo_Form_Login extends Zend_Form
{
    public function init()
    {
        $this->addElement('hidden', 'id', array('value' => '1', ));
        $this->addElement('text', 'pincode', array('label' => 'Код', ));
        $this->addElement('submit', 'submit', array('label' => 'Войти', 'ignore' => true, ));
        $this->setMethod('post');
        $this->setAction('');
        $this->setAttrib('id', 'form');
    }
}