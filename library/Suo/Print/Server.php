<?php
 /**
  * Сервер на сокетах.
  * Слушает сокет, адрес и порт которого заданы в config.inc.php, принимает соединения, создает Обмен и
  * совершает операции чтения/записи данных.
  *
  * @author Ilya Garaga
  */

require_once 'Exchange.php';

class Suo_Print_Server
{
    /**
     * Запуск сервера.
     * Создается сокет и ожидаются подключения клиентов.
     *
     * @param string $address Адрес для подключения
     * @param int $port Порт для подключения
     * @return resource|null Ресурс созданного сокета
     */
    public function start($address, $port)
    {
        $server_socket = null;
        echo "Suo_Print_Server starts at " . date('d.m.Y H:i:s') . "\n";
        echo "Test database connection\n";
        echo "\nTest socket connection\n";
        // establish connection
        $server_socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
        if (false === $server_socket) {
            echo "socket_create failed\n";
        } else if (false === socket_bind($server_socket, $address, $port)) {
           socket_close($server_socket);
           echo "socket_bind failed address " . $address . " port " . $port . "\n";
        } else if (false === socket_listen($server_socket, 50)) {
            socket_close($server_socket);
            echo "socket_listen failed\n";
        } else {
            echo "socket_create OK\n";
            echo "socket_bind   OK\n";
            echo "socket_listen OK\n";
            echo "\nServer waits for incoming connections...\n";
            $this->acceptClients($server_socket);
        }
        return $server_socket;
    }

    /**
     * Остановка сервера
     * @param resource $server_socket Ресурс сокета
     * @return void
     */
    public function finish($server_socket) {
        if ($server_socket) {
            socket_close($server_socket);
        }
        echo "Suo_Print_Server finished at " . date("d.m.Y H:i:s") . "\n";
    }

    /**
     * Чтение данных из сокета
     *
     * @return false|string Прочитанные данные или false, если операция завершилась по timeout
     */
    public function read()
    {
        $socket = $this->_client_socket;
        $is_timeout = false;
        $start = microtime(true);
        $read = false;
        socket_set_nonblock($socket);
        do {
            // 10 seconds for socket read operation
            if (10 < (microtime(true) - $start)) {
                $is_timeout = true;
                break;
            }
            $read = @socket_read($socket, 2048, PHP_NORMAL_READ);
        } while (false === $read);
        socket_set_block($socket);
        if (true == $is_timeout) {// something wrong on reading data
            echo "Suo_Print_Server read() timeout.\n";
        }
        return $read;
    }

    /**
     * Запись данных в сокет
     *
     * @param string $message Строка для записи
     * @return int Количество записанных данных
     */
    public function write($message)
    {
        $len = socket_write($this->_client_socket, $message, strlen($message));
        return $len;
    }

    /**
     * Клиентский сокет
     * Создается при принятии соединения от клиента и используется в операциях чтения/записи.
     *
     * @var resource
     */
    private $_client_socket;

    /**
     * Ожидание подключения клиентов
     *
     * @param resource $server_socket Сокет сервера
     * @return void
     */
    private function acceptClients($server_socket)
    {
        do {
            // listen for client
            $client_socket = socket_accept($server_socket);
            if (false === $client_socket) {
                echo "socket_accept() failed at " . date("d.m.Y H:i:s") . "\n";
            }  else {
                $socket_name = '';
                if (false === socket_getpeername($client_socket, $socket_name)) {
                    echo "INF Suo_Print_Server accept client with error. Close client socket.\n";
                    socket_close($client_socket);
                } else {
                    echo "INF Suo_Print_Server accept client with IP: " . $socket_name . " at " . date('d.m.Y H:i:s') . "\n";
                    $this->_client_socket = $client_socket;
                    $exchange = new Suo_Print_Exchange($this);
                    $stop_server = $exchange->exchange();
                    unset($exchange);
                    socket_close($client_socket);
                    echo "INF Suo_Print_Server close client with IP: " . $socket_name . " at " . date('d.m.Y H:i:s') . "\n";
                    if (true == $stop_server) { // one of client sent message to stop server
                        break;
                    }
                }
            }
        } while (true);
    }
}
?>