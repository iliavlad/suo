<?php

$address = '127.0.0.1';
$port = 62500;

$stopmessage = 'stopserver';

echo "Stop server starts at " . date('d.m.Y H:i:s') . "\n";

$socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);

if (false === $socket) {
    echo "socket_create failed\n";
} else {
    if (false === socket_connect($socket, $address, $port)) {
        echo "socket_connect failed\n";
    } else {
        echo "Send stop message\n";
        socket_write($socket, $stopmessage);
    }
    socket_close($socket);
}

echo "Stop server ends at " . date('d.m.Y H:i:s') . "\n";