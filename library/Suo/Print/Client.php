<?php

class Suo_Print_ClientException extends Suo_Exception
{
    const SUO_PRINT_SOCKET_CREATE_FAILED    = 1;
    const SUO_PRINT_SOCKET_CONNECT_FAILED   = 2;
    const SUO_PRINT_SOCKET_WRITE_FAILED     = 3;

    protected $_messages = array(
        self::SUO_PRINT_SOCKET_CREATE_FAILED    => 'socket_create failed. error %s',
        self::SUO_PRINT_SOCKET_CONNECT_FAILED   => 'socket_connect failed. error %s',
        self::SUO_PRINT_SOCKET_WRITE_FAILED     => 'socket_write failed. error %s',
    );
}

class Suo_Print_Client
{
    const DATA_SEPARATOR = ':::';
    const STOP_MESSAGE   = 'stop';

    public function send($address, $port, array $data)
    {
        $message = '';
        foreach ($data as $key => $value) {
            $message .= "$key=>$value" . self::DATA_SEPARATOR;
        }
        $message .= self::STOP_MESSAGE;
        $socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
        if (false === $socket) {
            throw new Suo_Print_ClientException('SUO_PRINT_SOCKET_CREATE_FAILED', socket_strerror(socket_last_error()));
        }
        if (false === socket_connect($socket, $address, $port)) {
            socket_close($socket);
            throw new Suo_Print_ClientException('SUO_PRINT_SOCKET_CONNECT_FAILED', socket_strerror(socket_last_error()));
        }
        if (false === socket_write($socket, $message)) {
            socket_close($socket);
            throw new Suo_Print_ClientException('SUO_PRINT_SOCKET_WRITE_FAILED', socket_strerror(socket_last_error()));
        }
        socket_close($socket);
    }
}