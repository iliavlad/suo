<?php
/**
 * Обрабока сообщений сервера
 *
 * Клиент подключается к серверу и присылает ему сообщения
 * На сообщение stop Сервер должен остановиться
 * Другие сообщения содержат данные, которые надо распечатать
 *
 * @author Илья Гарага
 *
 */

require_once 'Word.php';

class Suo_Print_Exchange
{
    const DATA_SEPARATOR = ':::';
    const STOP_MESSAGE   = 'stop';
    const STOP_SERVER    = 'stopserver';
    const OK             = 'OK';

    public function __construct(Suo_Print_Server $server)
    {
        $this->_server = $server;
    }

    public function exchange()
    {
        $stop = $this->read();
        while (false !== $stop) {
            $this->write(self::OK);
            $stop = $this->read();
        }
        $this->close();
        return $this->_stop_server;
    }

    private function read()
    {
        $read = $this->_server->read();
        if ($read) {
            $data = explode(self::DATA_SEPARATOR, $read);
            if (self::STOP_MESSAGE == $data[count($data) - 1]) {
                $data = array_splice($data, 0, -1);
                $read = false;
            } else if (self::STOP_SERVER == $data[count($data) - 1]) {
                $this->_stop_server = true;
                $read = false;
                $data = array_splice($data, 0, -1);
            }
            $data_exloded_keys = array();
            foreach ($data as $data_value) {
                list($key, $value) = explode('=>', $data_value);
                $data_exloded_keys[$key] = $value;
            }
            $this->_check_data = $data_exloded_keys;
        }
        return $read;
    }

    private function write($message)
    {
        $this->_server->write($message);
    }

    private function close()
    {
        $this->printToWord($this->_check_data);
        return $this->_stop_server;
    }

    /**
     * Сервер
     *
     * @var Suo_Print_Server
     */
    protected $_server = null;

    /**
     * Значение, которое возвращается функцией exchange()
     * Если приняли сообщение об остановке сервера, то возвращаем его и сервер останавливается.
     *
     * @var bool
     */
    protected $_stop_server = false;

    /**
     * Данные талона
     * Данные считываются при обмене с клиентом, затем открывается файл шаблона, данные заносятся в файл и файл распечатывается
     *
     * @var array
     */
    protected $_check_data = array();

    protected function printToWord(array $data)
    {
        if (!empty($data)) {
            $print = new Suo_Print_Word();
            $print->printCheck($data);
        }
    }
}