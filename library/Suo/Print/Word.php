<?php

class Suo_Print_Word
{
    public $filename = 'c:\tmp\print.doc';

    public function printCheck(array $data)
    {
        $keys = array_keys($data);

        $word = null;
        try {
            $word = new COM('word.application');
            $word->Visible = false;

            $doc = $word->Documents->Open($this->filename, false, false, false);//, '', '', true, '', '', 0, 1251, false);
            $count = $doc->Fields->Count;
            $codes = array();
            for ($i = 1; $i <= $count; $i++) {
                $code = trim($doc->Fields[$i]->Code->Text);
                $codes[] = $code;
                if (false !== strpos($code, 'QUOTE')) {
                    foreach ($keys as $key_field) {
                        if (false !== strpos($code, $key_field)) {
                            $doc->Fields[$i]->Result->Text = (string)$data[$key_field];
                        }
                    }
                }
            }
            $doc->PrintOut(true);

            while ($word->BackgroundPrintingStatus > 0) {
                sleep(0.5);
            }
            //var_dump($codes);
            $doc->Close(0); // 0 - wdDoNotSaveChanges, -1 - wdSaveChanges
            $doc = null;
        } catch (Exception $e) {
            echo iconv('CP1251', 'CP866', date('d.m.Y H:i:s') . ' ' . $e->getLine() . ' ' . $e->getMessage());
        }
        if ($word) {
            try {
                $word->Quit(0);
            } catch (Exception $e) {
                echo iconv('CP1251', 'CP866', date('d.m.Y H:i:s') . ' ' . $e->getLine() . ' ' . $e->getMessage());
            }
        }
        $word = null;
    }
}