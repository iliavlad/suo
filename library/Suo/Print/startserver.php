<?php

require_once 'Server.php';

$address = '127.0.0.1';
$port = 62500;

$server = new Suo_Print_Server();
$socket = $server->start($address, $port);
$server->finish($socket);