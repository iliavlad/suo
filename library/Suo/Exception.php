<?php
/**
 * Класс для работы с исключениями.
 * Каждый класс определяет свои константы и массив строк (свойство $_messages с доступом protected)
 */
require_once 'Zend/Exception.php';

class Suo_Exception extends Zend_Exception
{
    const INFORM_EXCEPTION_NO_EXCEPTION         = 0;
    const INFORM_EXCEPTION_CODE_NOT_DEFINED     = 1;
    const INFORM_PROPERTY_MESSAGES_NOT_EXISTS   = 2;

    private $_messages_base = array(
        self::INFORM_EXCEPTION_NO_EXCEPTION => 'Нет исключения.',
        self::INFORM_EXCEPTION_CODE_NOT_DEFINED => 'Не определено исключение с кодом %s.',
        self::INFORM_PROPERTY_MESSAGES_NOT_EXISTS => 'В наследуемом классе необходимо переопределить свойство $_messages с доступом protected.',
    );

    public function __construct($str_code)
    {
        $code = self::INFORM_EXCEPTION_NO_EXCEPTION;
        $message = $this->_messages_base[$code];
        // проверяем, что определен массив $_messages.
        if (!property_exists($this, '_messages')) {
            $code = self::INFORM_PROPERTY_MESSAGES_NOT_EXISTS;
            $message = $this->_messages_base[$code];
        } else {
            $code = constant(get_class($this) . "::$str_code");
            // проверяем, что переданная константа определена в классе.
            if (null == $code) {
                $code = self::INFORM_EXCEPTION_CODE_NOT_DEFINED;
                $message = sprintf($this->_messages_base[$code], $str_code);
            } else {
                $message = $this->_messages[$code];
                // смотрим, были ли переданы параметры для форматной сроки
                if (func_num_args() > 1) {
                    $params = func_get_args(); // получаем все параметры
                    $params[0] =  $message; // и вместо первого (кода) подставляем форматную строку
                    $message = call_user_func_array('sprintf', $params); // преобразуем сообщение
                }
            }
        }
        if (defined('TEST_CONFIG')) {
            $message = iconv('UTF-8', 'CP866', $message);
        }
        $message .= ' file: ' . $this->getFile() . ' line: ' . $this->getLine();
        parent::__construct($message, $code);
    }
}