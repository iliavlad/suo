<?php

define('VERSION', '0.2');

class SuoOperator extends GtkWindow
{
	public function __construct()
	{
		parent::__construct();
		$this->connect_simple('destroy', array($this, 'quit'));

        $this->_config_path = dirname(__FILE__);
        $this->loadConfig();

        $this->_db = $this->dbConnect();

        $window_config = $this->_config['window'];

		$this->set_title($window_config['title']);
        $this->set_size_request($window_config['width'], $window_config['height']);
        $this->move($window_config['x_position'], $window_config['y_position']);

        $this->set_resizable(FALSE);

        $pixbuf = $this->render_icon(Gtk::STOCK_APPLY, Gtk::ICON_SIZE_DIALOG);
        $this->set_icon($pixbuf);

        $this->set_keep_above(true);

        $font = new PangoFontDescription('Bold');
        $font->set_size(12);
        $this->_font = $font;

        $this->_login_screen = $this->makeLoginScreen();

        $this->_ticket_screen = $this->makeTicketScreen();

        $version_compare = $this->getServerOperatorVersion();

        $vbox = new GtkVBox();

        if (0 == $version_compare) {
            $vbox->pack_start($this->_login_screen);
            $vbox->pack_start($this->_ticket_screen);
        } else {
            $vbox->pack_start($this->makeVersionScreen());
        }

        $this->add($vbox);
		$this->show_all();
        $this->_ticket_screen->hide();
	}

	public function quit()
    {
        $this->stopTimer();
        $this->saveConfig();
        Gtk::main_quit();
    }

    private function getServerOperatorVersion()
    {
        $result = -1;
        $q = "SELECT `version` FROM `operator_version`";
        $res = mysql_query($q);
        if ($res) {
            $row = mysql_fetch_assoc($res);
            $version = $row['version'];
            if (trim($version) === VERSION) {
                $result = 0;
            }
        }
        return $result;
    }

    // Конфиг
    private $_config_path = '.';
    private $_config_file_name = 'operator.ini';

    private $_config = array(
        // Параметры окна
        'window' => array(
            'title' => 'Вход не выполнен',
            'width' => '300',
            'height' => '200',
            'x_position' => '0',
            'y_position' => '0',
        ),
        // Параметры соединения с базой данных
        'db' => array(
            'host' => 'localhost',
            'base' => 'suo',
            'user' => 'suo',
            'pass' => '',
        ),
        // Оператор
        'operator' => array(
            'id' => '1',
            'timer' => '2000',
            'next_plus_position' => '3',
        ),
    );

    private function loadConfig()
    {
        if (file_exists($this->_config_path . '/' . $this->_config_file_name)) {
            $config = parse_ini_file($this->_config_path . '/' . $this->_config_file_name, true);
            foreach ($config as $group => $group_data) {
                if (isset($this->_config[$group])) {
                    foreach ($group_data as $param => $value) {
                        if (isset($this->_config[$group][$param])) {
                            $this->_config[$group][$param] = $value;
                        }
                    }
                }
            }
        }
        $user_id = '-1';
        if (isset($config['operator']) && isset($config['operator']['id'])) {
            $user_id = $config['operator']['id'];
        }
        $this->_user_id = $user_id;
        if (isset($config['operator']) && isset($config['operator']['timer'])) {
            $this->_timer_timeout = $config['operator']['timer'];
        }
        if (isset($config['operator']) && isset($config['operator']['next_plus_position'])) {
            $this->_next_plus_position = intval($config['operator']['next_plus_position']);
        }
    }

    private function saveConfig()
    {
        $this->_config['operator']['id'] = $this->_user_id;
        $window_position = $this->get_position();
        $this->_config['window']['x_position'] = $window_position[0];
        $this->_config['window']['y_position'] = $window_position[1];
        $file_content = '';
        foreach ($this->_config as $group_name => $group_data) {
            $file_content .= "[$group_name]\n";
            foreach ($group_data as $element_name => $element_value) {
                $file_content .= $element_name . ' = "' . $element_value . '"' . "\n";
            }
        }
        file_put_contents($this->_config_path . '/' . $this->_config_file_name, $file_content);
    }

    // Экраны
    protected $_font = null;

    // Экран входа
    protected $_login_screen = null;
    protected $_ticket_screen = null;

    protected $_user_id_combobox = null;
    protected $_user_pin_entry = null;
    protected $_message_label = null;
    protected $_ticket_label = null;
    protected $_ticket_label_start_text = 'В очереди: ';
    protected $_client_label = null;
    protected $_client_label_wait_text = 'Ожидаем клиента №';
    protected $_client_label_accept_text = 'Приём клиента №';

    protected $_ticket_button_box = null;
    protected $_call_box = null;
    protected $_start_box = null;
    protected $_end_box = null;

    private function makeLoginScreen()
    {
		$mainBox  = new GtkVBox();

        $mainBox->pack_start($this->getSuoLabel('Выберите пользователя'), false, false);

        $combo = $this->getUserListCombo();
        $this->_user_id_combobox = $combo;
        $mainBox->pack_start($combo, false, false);

        $mainBox->pack_start($this->getSuoLabel('Введите код'), false, false);
        $entry = new GtkEntry();
        $entry->modify_font($this->_font);
        $this->_user_pin_entry = $entry;
        $mainBox->pack_start($entry, false, false);

        $label = $this->getSuoLabel();
        $mainBox->pack_start($label, false, false);
        $this->_message_label = $label;

		$mainBox->pack_start($this->getLoginButton(), false, false);
		$mainBox->pack_end($this->getQuitButton(), false, false);

        return $mainBox;
    }


    private function makeVersionScreen()
    {
        $mainBox  = new GtkVBox();
        $mainBox->pack_start($this->getSuoLabel('Необходимо обновить программу'), false, false);
        $mainBox->pack_end($this->getQuitButton(), false, false);
        return $mainBox;
    }

    private function makeTicketScreen()
    {
		$mainBox  = new GtkVBox();

        $tlabel = $this->getSuoLabel();
        $mainBox->pack_start($tlabel, false, false);
        $this->_ticket_label = $tlabel;

        $label = $this->getSuoLabel();
        $mainBox->pack_start($label, false, false);
        $this->_client_label = $label;

		$this->_ticket_button_box = new GtkVBox();

        // наборы кнопок
        $this->_call_box = new GtkVBox();
        $this->_call_box->pack_start($this->getCallButton(), false, false);

        $this->_start_box = new GtkVBox();
        $this->_start_box->pack_start($this->getStartButton(), false, false);
        $this->_start_box->pack_start($this->getNextButton(), false, false);

        $this->_end_box = new GtkVBox();
        $this->_end_box->pack_start($this->getEndButton(), false, false);
        // окончание наборов кнопок

        $this->_ticket_button_box->pack_start($this->_call_box, false, false);
        $this->_ticket_button_box->pack_start($this->_start_box, false, false);
        $this->_ticket_button_box->pack_start($this->_end_box, false, false);
        $this->_start_box->hide();
        $this->_end_box->hide();

        $mainBox->pack_start($this->_ticket_button_box, false, false);

        $label = $this->getSuoLabel();
        $mainBox->pack_start($label, false, false);

		$mainBox->pack_end($this->getQuitButton(), false, false);
		$mainBox->pack_end($this->getChangeUserButton(), false, false);

        return $mainBox;
    }

    private function getSuoLabel($text = '')
    {
        $label = new GtkLabel($text);
        $label->modify_font($this->_font);
        return $label;
    }

    private function getSuoButton($caption, $callback_function)
    {
        $btn = new GtkButton($caption);
        $btn->connect('clicked', array($this, $callback_function));
        $btn->modify_font($this->_font);
        return $btn;
    }

    private function getLoginButton()
    {
        $btn = $this->getSuoButton('Войти', 'onLoginClicked');
        $lbl = $this->getSuoLabel('aaa');
        $btn->set_image($lbl);
        $btn->set_border_width(2);
        return $btn;
    }

    private function getQuitButton()
    {
        $btn = $this->getSuoButton('Выйти', 'quit');
        return $btn;
    }

    private function getChangeUserButton()
    {
        $btn = $this->getSuoButton('Сменить пользователя', 'onChangeUserClicked');
        return $btn;
    }

    private function getCallButton()
    {
        $btn = $this->getSuoButton('Вызвать', 'onCallClicked');
        return $btn;
    }

    private function getStartButton()
    {
        $btn = $this->getSuoButton('Начать', 'onStartClicked');
        return $btn;
    }

    private function getNextButton()
    {
        $btn = $this->getSuoButton('Следующий', 'onNextClicked');
        return $btn;
    }

    private function getEndButton()
    {
        $btn = $this->getSuoButton('Завершить', 'onEndClicked');
        return $btn;
    }


    private function getUserListCombo()
    {
        $combo = GtkComboBox::new_text();
        $rows = $this->fetchAllUsers();
        $user_list = array();
        if ($rows) {
            $active_index = -1;
            while (false !== ($row = mysql_fetch_assoc($rows))) {
                $name = $row['last_name'] . ' ' .
                        substr($row['first_name'], 0, 1) . '. ' .
                        substr($row['parent_name'], 0, 1) . '.';
                $combo->append_text($row['room_number'] . '. ' . $name);
                $user_list[] = array('operator_id' => $row['operator_id'], 'room_id' => $row['room_id'], 'pin' => $row['pincode'], 'name' => $name, );
                if ($row['operator_id'] === $this->_user_id) {
                    $active_index = count($user_list) - 1;
                }
            }
            $combo->set_active($active_index);
        }
        $this->_user_list = $user_list;
        return $combo;
    }

    public function onLoginClicked()
    {
        $combo_index = $this->_user_id_combobox->get_active();
        $user = $this->_user_list[$combo_index];
        $pin = $this->_user_pin_entry->get_text();
        if ($pin !== $user['pin']) {
            $this->_message_label->set_text('Неверный код');
        } else {

            $this->_user_pin_entry->set_text('');
            $this->_user_id = $user['operator_id'];
			$this->_room_id = $user['room_id'];
            $this->_login_screen->hide();
            $this->_ticket_screen->show();

            $this->set_title($user['name']);
            $this->getCurrentTickets();

            // вычисляем, какой экран будем показывать
            $this->_call_box->hide();
            $this->_start_box->hide();
            $this->_end_box->hide();
            if (self::WAITCLIENT == $this->_current_ticket_status) {
                $this->_client_label->set_text($this->_client_label_wait_text . $this->_current_check_code);
                $this->_start_box->show();
            } else if (self::ACCEPTED == $this->_current_ticket_status) {
                $this->_client_label->set_text($this->_client_label_accept_text . $this->_current_check_code);
                $this->_end_box->show();
            } else {
                $this->_client_label->set_text('');
                $this->_call_box->show();
            }

            // запускаем таймер, по которому будем обновлять количество клиентов в очереди
            $this->startTimer();
        }
        return '';
    }

    public function onChangeUserClicked()
    {
        $this->_ticket_screen->hide();
        $this->_login_screen->show();
        $this->stopTimer();
    }

    public function onCallClicked()
    {
        if (null !== $this->_current_ticket) {
            $this->setTicketStatusWait($this->_current_ticket);

            $this->_wait_time_start = time();

            $this->_client_label->set_text($this->_client_label_wait_text . $this->_current_check_code);

            $this->_call_box->hide();
            $this->_start_box->show();
        }
    }

    public function onStartClicked()
    {
        if (null !== $this->_current_ticket) {
			$ticket_id = $this->_current_ticket;
            $this->setTicketStatusAccept($ticket_id, $this->_user_id);

            $this->_client_label->set_text($this->_client_label_accept_text . $this->_current_check_code);

            $this->_start_box->hide();
            $this->_end_box->show();
        }
    }

    public function onNextClicked()
    {
        if (null !== $this->_current_ticket) {
            // обновляем позиции всех заявок, которые будут после текущей
            $q = "UPDATE `ticket` SET `queue_position`=(`queue_position`+1) WHERE `queue_id`='{$this->_current_queue_id}' AND " .
                "`queue_position`>=" . ($this->_current_queue_position + $this->_next_plus_position);
            $res = mysql_query($q);
            // обновляем позицию текущей заявки и делаем ее новой
            $status_id = self::NEWTICKET;
            $position = $this->_current_queue_position + $this->_next_plus_position;
			$emp_add = '';
			if (true == $this->_employee_id_was_null) {
				$emp_add = '`employee_id`=NULL, ';
			}
            $q = "UPDATE `ticket` SET `status_id`='$status_id', $emp_add `queue_position`='$position' WHERE `id`='{$this->_current_ticket}'";
            $res = mysql_query($q);
            $q = "INSERT INTO `ticket_archive` (`data_date`, `who_group`, `who_id`, `ticket_id`, `status_id`, `what`) " .
                "VALUES ('" . date('Y-m-d H:i:s') . "', 'operator', '" . $this->_user_id . "', '" . $this->_current_ticket . "', " .
                    "'$status_id', 'Установлен статус')";
            $res = mysql_query($q);

            $this->getCurrentTickets();
            // обновляем статус текущей заявки как ожидаемой
            $this->onCallClicked();
        }
    }

    public function onEndClicked()
    {
        if (null !== $this->_current_ticket) {
            $this->_client_label->set_text('');
            $this->setTicketStatusClose($this->_current_ticket);

            $this->_end_box->hide();
            $this->_call_box->show();
        }
    }

    // Работа с базой
    // TODO Переделать для нормальной работы с базой
    const NEWTICKET     = 1;
    const INQUEUE       = 2;
    const ACCEPTED      = 3;
    const DECLINED      = 4;
    const CLOSED        = 5;
    const WAITCLIENT    = 6;

    protected $_db = null;
    protected $_user_list = array();
    protected $_user_id = '1';
	protected $_room_id = '1';

    protected $_current_ticket = null;
    protected $_current_check_code = null;
    protected $_current_ticket_status = self::NEWTICKET;
    protected $_current_queue_id = null;

    protected $_next_plus_position = 3;
    protected $_current_queue_position = 0;

	protected $_employee_id_was_null = false;

    private function dbConnect()
    {
        $config = $this->_config['db'];
        $res = mysql_connect($config['host'], $config['user'], $config['pass']);
        mysql_select_db($config['base']);
        return $res;
    }

    private function fetchAllUsers()
    {
        $q = "SELECT `employee_has_room`.`employee_id` as `operator_id`, `employee_has_room`.`room_id` as `room_id`, " .
					"LPAD(`room`.`number`, 10, ' ') AS `room_number`, " .
					"`employee`.`pincode`, `employee`.`last_name`, `employee`.`first_name`, `employee`.`parent_name` " .
			 "FROM `employee_has_room` " .
			 "JOIN `employee` ON `employee`.`id`=`employee_has_room`.`employee_id` " .
			 "JOIN `room` ON `room`.`id`=`employee_has_room`.`room_id` " .
			 "ORDER BY `room_number`, `employee`.`last_name`, `employee`.`first_name`";
        $rows = mysql_query($q, $this->_db);
        return $rows;
    }

    private function getCurrentTickets()
    {
        $ticket_count = 0;
        $user_id = $this->_user_id;
		$room_id = $this->_room_id;
        if ($user_id > 0) {
            $date = date('Y-m-d');
            $q = "SELECT * " .
                 "FROM `queue` " .
                 "WHERE (`employee_id`='$user_id' OR (`employee_id` IS NULL AND `room_id`='$room_id')) AND DATE(`start_date`)='$date' " .
                 "LIMIT 1";
            $res_queue = mysql_query($q);
            if ($res_queue && mysql_num_rows($res_queue) > 0) {
                $row = mysql_fetch_assoc($res_queue);
                $queue_id = $row['id'];
                $this->_current_queue_id = $queue_id;
                $q = "SELECT * " .
                     "FROM `ticket` " .
                     "WHERE `queue_id`='$queue_id' AND `status_id`<>' " . self::CLOSED . "' AND `status_id`<>'" . self::DECLINED . "' " .
                     "ORDER BY `queue_position`";
                $res_tickets = mysql_query($q);
                if ($res_tickets) {
                    $ticket_count = mysql_num_rows($res_tickets);
                    $row = mysql_fetch_assoc($res_tickets);
                    $this->_current_ticket = $row['id'];
                    $this->_current_ticket_status = $row['status_id'];
                    $this->_current_queue_position = $row['queue_position'];

                    $check_id = $row['check_id'];
                    $q = "SELECT `code` FROM `check` WHERE `id`='$check_id'";
                    $res = mysql_query($q);
                    if ($res) {
                        $row = mysql_fetch_assoc($res);
                        $this->_current_check_code = $row['code'];
                    }
                }
            }
        }
        $this->_ticket_label->set_text($this->_ticket_label_start_text . $ticket_count);
    }

    private function updateTicketStatus($ticket_id, $status_id, $employee_id = null)
    {
        $queue_set = "";
        if (self::CLOSED == $status_id) {
            $queue_set = ", `queue_id`=NULL ";
        }
        $employee_set = '';
        if (null != $employee_id) {
            $employee_set = ", `employee_id`='$employee_id' ";
        }
        $q = "UPDATE `ticket` SET `status_id`='$status_id' $queue_set $employee_set WHERE `id`='$ticket_id'";
        $res = mysql_query($q);
        $q = "INSERT INTO `ticket_archive` (`data_date`, `who_group`, `who_id`, `ticket_id`, `status_id`, `what`) " .
            "VALUES ('" . date('Y-m-d H:i:s') . "', 'operator', '" . $this->_user_id . "', '$ticket_id', '$status_id', 'Установлен статус')";
        $res = mysql_query($q);
        return true;
    }

    private function setTicketStatusNew($ticket_id)
    {
        return $this->updateTicketStatus($ticket_id, self::NEWTICKET);
    }

    private function setTicketStatusWait($ticket_id)
    {
        return $this->updateTicketStatus($ticket_id, self::WAITCLIENT);
    }

    private function setTicketStatusAccept($ticket_id, $employee_id = null)
    {
        return $this->updateTicketStatus($ticket_id, self::ACCEPTED, $employee_id);
    }

    private function setTicketStatusDecline($ticket_id)
    {
        return $this->updateTicketStatus($ticket_id, self::DECLINED);
    }

    private function setTicketStatusClose($ticket_id)
    {
        return $this->updateTicketStatus($ticket_id, self::CLOSED);
    }

    // Таймер
    protected $_timer_id = null;
    protected $_timer_timeout = 2000;
    protected $_wait_time_start = 0;

    private function startTimer()
    {
        $this->_timer_id = Gtk::timeout_add($this->_timer_timeout, array($this, 'onTimer'));
    }

    private function stopTimer()
    {
        if (null != $this->_timer_id) {
            Gtk::timeout_remove($this->_timer_id);
            $this->_timer_id = null;
        }
    }

    public function onTimer()
    {
        $this->getCurrentTickets();

        if (self::WAITCLIENT == $this->_current_ticket_status) {
            if (0 == $this->_wait_time_start) {
                $this->_wait_time_start = time();
            }
            $sec = time() - $this->_wait_time_start;
            $min = intval($sec / 60);
            $sec -= $min * 60;
            $str = ' (' . sprintf('%d', $min) . ':' . sprintf('%02d', $sec) . ')';

            $this->_client_label->set_text($this->_client_label_wait_text . $this->_current_check_code . $str);
        }

        $this->startTimer();
    }
}

new SuoOperator();
Gtk::main();