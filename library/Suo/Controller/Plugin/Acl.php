<?php
/**
 * Проверка доступа к страницам и перевод пользователя, если доступ закрыт.
 *
 * @author Ilya Garaga <>
 * @since 02.04.2010
 * @package Suo
 *
 */
class Suo_Controller_Plugin_Acl extends Zend_Controller_Plugin_Abstract
{
    /**
     * Конструктор
     *
     * @param Zend_Acl $acl Список доступа
     */
    public function __construct(Zend_Acl $acl)
    {
        $this->_acl = $acl;
    }

    /**
     * Проверяем доступа
     * Определяем, был ли сделан вход в систему, и переводим пользователя в нужные разделы по результатам проверки
     *
     * @param Zend_Controller_Request_Abstract $request Запрос к системе
     * @return void
     */
    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {
        $module = $request->getModuleName();
        $aAuthedModule = array('operator', 'admin', );
        if (in_array($module, $aAuthedModule)) {
            $auth = Zend_Auth::getInstance();
            // Определяем роль пользователя.
            // Если не авторизирован - значит "гость"
            $role = 'guest';
            if ($auth->hasIdentity()) {
                $user = $auth->getIdentity();
                if ('1' == $user->id) {
                    $role = 'admin';
                } else {
                    $role = 'operator';
                }
            }
            if ('operator' == $module) {
                if ('operator' != $role) {
                    $request->setModuleName('operator')
                        ->setControllerName('index')
                        ->setActionName('index');
                }
            } else {
                if ('admin' != $role) {
                    $request->setModuleName('default')
                        ->setControllerName('login')
                        ->setActionName('index');
                }
            }
        }
    }

    /**
     * @var Zend_Acl
     */
    protected $_acl = null;
}