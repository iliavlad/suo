<?php
class Suo_Controller_Operator_Base extends Suo_Controller_Base
{
    public function init()
    {
        parent::init();

        $operator = Zend_Auth::getInstance()->getIdentity();
        $this->_operator_id = $operator->id;

        $tickets = $this->getTickets();

        // данные view
        $operator_title = $operator->last_name . ' ' .
            mb_substr($operator->first_name, 0, 1, 'UTF-8') . '. ' .
            mb_substr($operator->parent_name, 0, 1, 'UTF-8') . '.';
        $suo_operator_session = new Zend_Session_Namespace('Suo_Operator');
        $room_title = 'Кабинет ' . $suo_operator_session->room_number;
        $this->view->title = $operator_title . ' ' . $room_title;

        $this->view->ticket_count = count($tickets);
        $this->view->tickets = $tickets;
		$this->view->max_today_records = $suo_operator_session->max_today_records;
    }

    protected function getTickets($need_update = false)
    {
        if (empty($this->_tickets) || true == $need_update) {
            $this->_ticket_gateway = new Suo_Model_TicketGateway();
            $suo_operator_session = new Zend_Session_Namespace('Suo_Operator');
            $room_id = $suo_operator_session->room_id;
            $this->_tickets =
                    $this->_ticket_gateway->fetchTicketsAndChecksNumbersByOperator($this->_operator_id, $room_id);
        }
        return $this->_tickets;
    }

    /**
     *
     * @var string
     */
    protected $_operator_id = '';

    /**
     *
     * @var array
     */
    protected $_tickets = array();

    protected $_ticket_gateway = null;
}