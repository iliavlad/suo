<?php

class Suo_Controller_Base extends Zend_Controller_Action
{
    public function __call($method, $args)
    {
        if ('Action' == substr($method, -6)) {
            // Если метод действия не был найден, то производится
            // переход к действию index
            return $this->_forward('index');
        }

        // все другие методы бросают исключение
        throw new Exception('Invalid method "'
                            . $method
                            . '" called',
                            500);
    }

    public function init()
    {
        $this->view->data_name = $this->getName();
        $this->view->module_name = $this->getModuleName();
    }

    public function indexAction()
    {
        $this->_forward('list');
    }

    public function listAction()
    {
        $name = $this->_data_name;
        $class_name = 'Suo_Model_' . $name . 'Gateway';
        $gateway = new $class_name();
        $set = $gateway->fetchAll();
        $this->view->set = $set;
    }

    public function editAction()
    {
        $request = $this->getRequest();
        $id = $request->getUserParam('id', null);
        if (null === $id && !$request->isPost()) {
            $this->_forward('list');
        } else {
            $name = $this->getName();
            $class_name = 'Suo_Model_' . $name . 'Gateway';
            /* @var $gateway Suo_Model_Base_DataGateway */
            $gateway = new $class_name();
            if (!$request->isPost()) {
                /* @var $object Suo_Model_Base_ViewData */
                $object = $gateway->fetchById($id);
                if (null == $object) {
                    $this->_forward('list');
                } else {
                    $route_edit = strtolower($this->getName()) . '_edit';
                    $action = $this->view->url(array('id' => $id, ), $route_edit);
                    /* @var $form Suo_Model_Form_Base */
                    $form = $object->getForm();
                    $form->setAction($action);
                    $this->view->object_form = $form;
                }
            } else {
                $params = array();
                /* @var $object Suo_Model_Base_ViewData */
                $object = $gateway->create($params);
                $form = $object->getForm();
                if ($form->isValid($request->getPost())) {
                    $object->populate($form->getValues(), true);
                    $object->save();
                    return $this->_helper->redirector('list');
                } else {
                    $id = $form->getValue('id');
                    $route_edit = strtolower($this->getName()) . '_edit';
                    $action = $this->view->url(array('id' => $id, ), $route_edit);
                    $form->setAction($action);
                    $this->view->object_form = $form;
                }
            }
        }
    }

    public function addAction()
    {
        $name = $this->getName();
        $class_name = 'Suo_Model_' . $name . 'Gateway';
        $gateway = new $class_name();
        $params = array();
        $object = $gateway->create($params);
        $route_edit = strtolower($this->getName()) . '_edit';
        $action = $this->view->url(array('id' => '', ), $route_edit);
        $form = $object->getForm();
        $form->setAction($action);
        $this->view->object_form = $form;
    }

    public function deleteAction()
    {
        $request = $this->getRequest();
        $id = $request->getUserParam('id', null);
        if (null === $id) {
            $this->_forward('list');
        } else {
            $name = $this->getName();
            $class_name = 'Suo_Model_' . $name . 'Gateway';
            $gateway = new $class_name();
            $object = $gateway->fetchById($id);
            if (null !== $object) {
                $object->delete();
            }
            $this->_redirect($this->getModuleName() . '/' . strtolower($this->getName()) . '/list');
        }
    }

    protected function getName()
    {
        return $this->_data_name;
    }

    protected function getModuleName()
    {
        return $this->getRequest()->getModuleName();
    }

    protected $_data_name = '';
}