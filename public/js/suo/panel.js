dojo.addOnLoad(prepareSound)
dojo.addOnLoad(function(){setInterval(getQueue, 2000);});
dojo.addOnLoad(getQueue);

var global_o_dojo_sound = null
var global_s_ring_tickets = '0'

function getQueue() {
    //The parameters to pass to xhrGet, the url, how to handle it, and the callbacks.
    s_ring_tickets = '0'
    o_ring_elem = dojo.byId('ringTickets')
    if (null != o_ring_elem) {
        s_ring_tickets = o_ring_elem.innerHTML
    }
    var xhrArgs = {
        url: "/suo/panel/index/list?ringTickets=" + s_ring_tickets,
        handleAs: "text",
        load: function(data) {
            if (data.length > 0) {
                var targetNode = dojo.byId("panel_status");
                targetNode.innerHTML = data
                o_ring_elem = dojo.byId('ringTickets')
                if (null != o_ring_elem) {
                    global_s_ring_tickets = o_ring_elem.innerHTML
                    if ('0' != global_s_ring_tickets) {
                        //playNote()
                    } else {
                        global_s_ring_tickets = '0'
                    }
                }
            }
        },
        error: function(error) {
            //targetNode.innerHTML = "An unexpected error occurred: " + error;
        }
    }
    //Call the asynchronous xhrGet
    var deferred = dojo.xhrGet(xhrArgs);
}

function prepareSound() {
    global_o_dojo_sound = new dojox.av.FLAudio({
        initialVolume: 1,
        autoPlay: false,
        isDebug: false,
        statusInterval: 500
    })
    global_o_dojo_sound.load({
        url: 'sound/note.wav',
        id: 'flash_note1'
    })
}

function playNote() {
    if (null != global_o_dojo_sound) {
        global_o_dojo_sound.doPlay({id:'flash_note1'})
    }
}