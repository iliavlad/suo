dojo.addOnLoad(function(){setInterval(updateQueue, 600000);});

function updateQueue()
{
    var date = new Date();
    var minutes = date.getMinutes();
    if (minutes < 10) {
      minutes = '0' + minutes;
    }
    dojo.byId("updateTime").innerHTML = date.getHours() + ':' + minutes;
    gridOperatorGrid.sort();
    gridRoomGrid.sort();
    gridTicketsGrid.sort();
}

function setPeriod(elem, period)
{
    dojo.query('.active').removeClass('active');
    dojo.addClass(elem, 'active');
    storeOperator.url = '/suo/statistic/index/operator/period/' + period;
    storeRoom.url = '/suo/statistic/index/room/period/' + period;
    gridOperatorGrid.sort();
    gridRoomGrid.sort();
}

function selectRed() {
    dojo.query('.redcheck').attr('checked', true);
}

function decline1(id) {
    alert(id);
}