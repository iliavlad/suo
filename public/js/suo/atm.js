﻿dojo.addOnLoad(function(){showPage(0);});
dojo.addOnLoad(dailyReload);
dojo.addOnLoad(checkTodayRecordsOnPage)

var dayroom = 0;
var dlgDayTimeoutId = -1;

var global_n_no_check_click_count = 0;

function showPage(value) {
    //The parameters to pass to xhrGet, the url, how to handle it, and the callbacks.
    var xhrArgs = {
        url: "/suo/atm/index/page/page/" + value,
        handleAs: "text",
        load: function(data) {
            var targetNode = dojo.byId("screenContainer");
            targetNode.innerHTML = data;
        },
        error: function(error) {
            //targetNode.innerHTML = "An unexpected error occurred: " + error;
        }
    }
    //Call the asynchronous xhrGet
    var deferred = dojo.xhrGet(xhrArgs);
}
function clickCheckButton(value)
{
    if ('0' == value) {
        var page = dojo.byId('page');
        showPage(page.getAttribute('value'));
        return;
    }
    dlgRecordDay.show();
	dlgDayTimeoutId = setTimeout(clickDayCancel, 20000);

    dayroom = value;
    var room = dojox.json.query("[?id='" + dayroom + "']", rooms.items);
    if (null != room && 1 == room.length) {
        dojo.byId('dayroomName').innerHTML = room[0].description;
    }

    //The parameters to pass to xhrGet, the url, how to handle it, and the callbacks.
    var xhrArgs = {
        url: "/suo/atm/index/tickets/room/" + value,
        handleAs: "json",
        load: function(data) {
            for (divId in data) {
                var div = dojo.byId('day' + divId);
                if (null != div) {
                    current = data[divId];
                    div.innerHTML = current['clients'];
                    if (0 != current['max'] && current['count'] >= current['max']) {
                        dojo.attr('btnDay' + divId, "disabled", true);
                    }
                }
            }
        },
        error: function(error) {
            //targetNode.innerHTML = "An unexpected error occurred: " + error;
        }
    }
    //Call the asynchronous xhrGet
    var deferred = dojo.xhrGet(xhrArgs);
}

function clickDayButton(day) {
    if (true != dlgGetTheCheck.open) {
        dlgRecordDay.hide();
        dlgGetTheCheck.show();
        setTimeout(function(){dlgGetTheCheck.hide();}, 7000);
    }

    //The parameters to pass to xhrGet, the url, how to handle it, and the callbacks.
    var xhrArgs = {
        url: '/suo/atm/index/check/room/' + dayroom + '/day/' + day,
        handleAs: "json",
        load: function(data) {
            for (divId in data) {
                var div = dojo.byId(divId);
                div.innerHTML = data[divId];
            }
            setTimeout(print, 600);
            global_n_no_check_click_count = 0;
			//alert(111);
        },
        error: function(error) {
            clickDayButton(day);
            //targetNode.innerHTML = "An unexpected error occurred: " + error;
        }
    }
    //Call the asynchronous xhrGet
    var deferred = dojo.xhrGet(xhrArgs);
}

function clickNoRecordButton(room) {
    dayroom = room;
    clickDayButton('25');
}

function clickDayCancel() {
    if (dlgRecordDay.open) {
        dlgRecordDay.hide();
    }
}

function dlgRecordDayOnHide() {
    if (-1 != dlgDayTimeoutId) {
        clearTimeout(dlgDayTimeoutId);
    }
}

function dailyReload() {
    var today = new Date();
    var tomorrow = new Date(today.getFullYear(), today.getMonth(), today.getDate() + 1, 0, 15);
    var diff = tomorrow.getTime() - today.getTime();
    setTimeout(function(){window.location.reload();}, diff);
}

function checkTodayRecordsOnPage() {
    var xhrArgs = {
        url: '/suo/atm/index/updaterooms',
        handleAs: 'json',
        load: function(o_rooms) {
            rooms = o_rooms
            a_rooms_with_today_records = dojo.filter(rooms.items, function(o_item) {
                return o_item.max_today_records > 0
            })
            if (a_rooms_with_today_records.length > 0) {
                dojo.forEach(a_rooms_with_today_records, function(o_entry, n_index) {
                    var o_elem = dojo.byId('roomMaxTodayClients-' + o_entry.id)
                    if (null != o_elem) {
                        dojo.byId('roomMaxTodayClients-' + o_entry.id).innerHTML = o_entry.max_today_records
                    }
                })
                updateTodayRecordsOnPage()
            }
        }
    }
    //Call the asynchronous xhrGet
    var deferred = dojo.xhrPost(xhrArgs)
    setTimeout(checkTodayRecordsOnPage, 5000)
}

function updateTodayRecordsOnPage() {
    //The parameters to pass to xhrGet, the url, how to handle it, and the callbacks.
    var o_post_data = {
        'rooms': []
    }
    dojo.forEach(a_rooms_with_today_records, function(o_item, n_index) {
        o_post_data.rooms.push(o_item.id)
    })
    var xhrArgs = {
        url: '/suo/atm/index/todayrecords',
        postData: dojo.toJson(o_post_data),
        handleAs: 'json',
        load: function(o_records) {
            dojo.forEach(o_records, function(o_item, n_index) {
                var o_elem_wrapper = dojo.byId('roomTodayClientsWrapper-' + o_item.room_id)
                if (null != o_elem_wrapper) {
                    var o_room = dojox.json.query("[?id='" + o_item.room_id + "']", rooms.items)[0]
                    if (parseInt(o_item['count']) >= parseInt(o_room.max_today_records)) {
                        o_elem_wrapper.innerHTML = 'На сегодня запись завершена.'
                        dojo.attr('btnRoom-' + o_item.room_id, 'disabled', true)
                    } else {
                        if ('На сегодня запись завершена.' == o_elem_wrapper.innerHTML) {
                            o_elem_wrapper.innerHTML = 'Уже записалось <span id="roomTodayClients-' + o_item.room_id +
                                '">' + o_item['count'] + '</span>.Максимум <span id="roomMaxTodayClients-' + o_item.room_id +
                                '">' + o_room.max_today_records + '</span>.'
                            dojo.attr('btnRoom-' + o_item.room_id, 'disabled', false)
                        }
                        dojo.byId('roomTodayClients-' + o_item.room_id).innerHTML = o_item['count']
                    }
                }
            })
        }
    }
    //Call the asynchronous xhrGet
    var deferred = dojo.xhrPost(xhrArgs)
}

function showNoCheck() {
    setTimeout(print, 700);
    global_n_no_check_click_count++;
    dojo.style('doubleprint', 'display', 'block');
    dojo.byId('doubleprintcount').innerHTML = global_n_no_check_click_count;
    if (global_n_no_check_click_count > 4) {
        dlgGetTheCheck.hide()
        dlgNoCheck.show()
        var xhrArgs = {
            url: '/suo/atm/action/fail',
            handleAs: 'json',
            load: function() {
            }
        }
        //Call the asynchronous xhrGet
        var deferred = dojo.xhrGet(xhrArgs)
    }
}

function onClickTemoraryLock() {
    dlgNoCheck.hide()
}
