﻿dojo.addOnLoad(function(){setInterval(getTicketsByClient, 5000);});

function closeWindow() {
    var over = dojo.byId('todaystop');
    if (true != over.checked) {
        window.close();
    } else {
        var xhrArgs = {
            url: '/suo/operator/index/stop',
            handleAs: "json",
            load: function(data) {
                window.close();
            },
            error: function(error) {
                //targetNode.innerHTML = "An unexpected error occurred: " + error;
            }
        }
        //Call the asynchronous xhrGet
        var deferred = dojo.xhrGet(xhrArgs);
    }
}

function setFocusOnLoad() {
    dojo.byId('focusFirst').focus();
}

function getTicketsByClient() {
    //The parameters to pass to xhrGet, the url, how to handle it, and the callbacks.
    var divTicketCount = dojo.byId('ticket_count');
    var ticketCount = divTicketCount.innerHTML;
    var xhrArgs = {
        url: '/suo/operator/ticket/byclient',
        handleAs: "json",
        load: function(data) {
            var len = data.items.length;
            if (len != ticketCount) {
                divTicketCount.innerHTML = data.items.length;
            
                var targetNode = dojo.byId('formChecks');
                targetNode.innerHTML = '';
                for (i = 0; i < len; i++) {
                    var button = dojo.create('button');
                    button.type = 'submit';
                    button.className = 'ticketbutton';
                    button.name = 'ticket';
                    button.value = data.items[i].id + '-' + data.items[i].code;
                    button.innerHTML = data.items[i].code;
                    targetNode.appendChild(button);
                }
            }
        },
        error: function(error) {
            //targetNode.innerHTML = "An unexpected error occurred: " + error;
        }
    }

    //Call the asynchronous xhrGet
    var deferred = dojo.xhrGet(xhrArgs);
}

function hideByClientForm() {
    var div = dojo.byId('divChecks');
    if (null != div) {
        var a = dojo.byId('toogleChecks');
        if ('block' == div.style.display) {
            div.style.display = 'none';
            a.innerHTML = '[+]';
        } else {
            div.style.display = 'block';
            a.innerHTML = '[-]';
        }
    }
}

function onClickBtnSetMaxTodayRecords() {
    var n_records = parseInt(dojo.byId('max_today_records').value)
    if (n_records <= 0) {
        alert('Пожалуйста, введите число больше 0.')
    } else {
        var xhrArgs = {
            url: '/suo/operator/ticket/setmaxrecords?records=' + n_records,
            handleAs: "json",
            load: function(data) {
            }
        }
        //Call the asynchronous xhrGet
        var deferred = dojo.xhrGet(xhrArgs);
    }
}