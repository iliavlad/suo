<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
    protected function _initRegisterNamespace()
    {
        $autoloader = Zend_Loader_Autoloader::getInstance();
        $autoloader->registerNamespace('Suo');
    }

    protected function _initRoutes()
    {
        $this->bootstrap('FrontController');
        $front = $this->getResource('FrontController');
        $router = $front->getRouter();
        $router->addRoute('timetable',
                new Zend_Controller_Router_Route('commission/timetable/set/week/:week/room/:room',
                    array('module' => 'commission', 'controller' => 'timetable', 'action' => 'set', 'week' => 'current', )));
        $router->addRoute('atm_page',
                new Zend_Controller_Router_Route('atm/:page',
                    array('module' => 'atm', 'controller' => 'index', 'action' => 'index', 'page' => '1', )));
        $data_list = array('operator', 'ticket', 'atm', 'panel', 'room', 'queue', 'action', 'role', 'check', 'ticketkit', 'status', 'client',
            'workday', );
        foreach ($data_list as $name) {
            $router->addRoute($name . '_edit',
                new Zend_Controller_Router_Route('admin/' . $name . '/edit/:id',
                    array('module' => 'admin', 'controller' => $name, 'action' => 'edit', )));
            $router->addRoute($name . '_delete',
                new Zend_Controller_Router_Route('admin/' . $name . '/delete/:id',
                    array('module' => 'admin', 'controller' => $name, 'action' => 'delete', )));
        }
    }

    protected function _initDb()
    {
        $res = $this->getApplication()->getOption('resources');
        $res_db = $res['db'];
        $db = Zend_Db::factory($res_db['adapter'], $res_db['params']);
        $db->setFetchMode(Zend_Db::FETCH_ASSOC);
        Zend_Db_Table_Abstract::setDefaultAdapter($db);
        Zend_Registry::set('db', $db);
    }

    /**
     * Bootstrap the view doctype
     *
     * @return void
     */
    protected function _initDoctype()
    {
        $this->bootstrap('view');
        $view = $this->getResource('view');
        $view->suo_title = 'Поликлиника';

        Zend_Dojo::enableView($view);
        $view->dojo()->enable()
                ->setLocalPath('/suo/js/dojo/dojo.js')
                ->addStyleSheetModule('dijit.themes.tundra');
    }

    /**
     * @return Zend_Application
     */
    protected function _initPlugins()
    {
        $this->bootstrap('FrontController');
        $front = $this->getResource('FrontController');

        $front->registerPlugin(new Suo_Controller_Plugin_Layout());

        return $this;
    }

    protected function _initZFDebug()
    {return;
        if ('development' === APPLICATION_ENV) {
            $autoloader = Zend_Loader_Autoloader::getInstance();
            $autoloader->registerNamespace('ZFDebug');

            $options = array(
                'plugins' => array(
                    'Variables',
                    'File' => array('base_path' => APPLICATION_PATH . "/../"),
                    'Memory',
                    'Time',
                    'Registry',
                    'Exception',
                    'Html',
                    'Database' => array('adapter' => Zend_Registry::get('db')),
                )
            );

            //throw new Exception(var_export($options, true));
            $debug = new ZFDebug_Controller_Plugin_Debug($options);

            $this->bootstrap('frontController');
            $frontController = $this->getResource('frontController');
            $frontController->registerPlugin($debug);
        }
    }
}