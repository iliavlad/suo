<?php

class Admin_Bootstrap extends Zend_Application_Module_Bootstrap
{
    protected function _initAcl()
    {
        $acl = new Zend_Acl();

        $plugin = new Suo_Controller_Plugin_Acl($acl);
        $front = Zend_Controller_Front::getInstance();
        $front->registerPlugin($plugin);
    }
}