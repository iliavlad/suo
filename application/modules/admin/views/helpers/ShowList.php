<?php

class Suo_View_Helper_ShowList extends Suo_View_Helper_Base
{
    public function showList($data_name, Suo_Model_Base_DataSet $data_set)
    {
        $result = '';
        /* @var $translator Zend_Translate */
        $translator = Zend_Registry::get('Zend_Translate');
        $result .= '<div><p>' . $translator->translate($data_name . ' list') . '</p></div><br />';
        $result .= '<div><a href="' . $this->view->baseUrl('admin/' . strtolower($data_name) . '/add') . '">Добавить</a></div><br />';
        if ($data_set->count() == 0) {
            $result .= '<div>' . $translator->translate('no ' . strtolower($data_name) . 's') . '</div>';
        } else {
            /* @var $current Suo_Model_Base_ViewData */
            $current = $data_set->current();
            $properties = $current->getProperties();
            $route_edit = strtolower($data_name) . '_edit';
            //$route_delete = strtolower($data_name) . '_delete';
            $result .= "<table border='1'>\n";
            $result .= "  <tr>\n";
            $prop_count = count($properties);
            $many = $current->getManyToMany();
            foreach ($properties as $property) {
                $end = '';
                $value = $current->$property;
                if (in_array($property, $many)) {
                    $end = 's';
                }
                $result .= '    <th>' . $translator->translate($property . $end) . '</th>' . "\n";
            }
            //$result .= '    <th></th>' . "\n";
            $result .= "  </tr>\n";
            foreach ($data_set as $data) {
                $result .= "  <tr onmouseover=\"this.className='hovered'\" onmouseout=\"this.className='null'\">\n";
                if (isset($data->id)) {
                    $href = $this->view->url(array('id' => $data->id, ), $route_edit);
                } else {
                    $href = '';
                }
                $id = 1;
                foreach ($properties as $property) {
                    $value = $data->$property;
                    if (null === $value) {
                        $value = ' ';
                    } else if (is_array($value)) {
                        $value = count($value);
                    }
                    $result .= '    <td><a href="' . $href . '">' . $value . '</a></td>' . "\n";
                }
                //$result .= '    <td><a href="' . $this->view->url(array('id' => $id, ), $route_delete) . '">' .
                    //$translator->translate('delete') . '</a></td>' . "\n";
                $result .= "  </tr>\n";
            }
            $result .= "</table>\n";
        }
        return $result;
    }
}