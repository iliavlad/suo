<?php

class Atm_Form_Selectday extends Zend_Dojo_Form
{
     /**
     * Form initialization
     *
     * @return void
     */
    public function init()
    {
        $this
            ->setMethod(Zend_Form::METHOD_POST)
            ->setAttribs(
                array(
                    'id'   => 'formSelectDay',
                    'name' => 'formSelectDay'
                )
            );

        $this->addElement(
                'SubmitButton',
                'noCheck',
                array(
                    'dijitParams'    => array(
                        'jsId'           => 'noCheck',
                        'required'       => false,
                        'label'          => 'нет талона',//Если нет талона, нажмите кнопку',
                    )
                )
        );
        $this->setElementDecorators(
            array(
                array('DijitElement'),
                array('Errors'),
                array('Description'),
                array('Label', array('separator' => ' ')),
                array('HtmlTag', array('tag' => 'div', 'class' => 'elementGroup clear')),
            ),
            array(),
            false
        );
    }
}