<?php

class Atm_IndexController extends Suo_Controller_Base
{
    public function indexAction()
    {
        $request = $this->getRequest();
        $addr = $request->getServer('REMOTE_ADDR');
        $gateway = new Suo_Model_AtmGateway();
        $rooms = $gateway->getRoomsByAtmAddr($addr);
        $dojoData = new Zend_Dojo_Data('id', $rooms);
        $this->view->rooms = $dojoData->toJson();
    }

    public function pageAction()
    {
        $this->_helper->layout->disableLayout();

        $page_limit = 12;
        $page = 0;
        $request = $this->getRequest();
        $addr = $request->getServer('REMOTE_ADDR');
        $gateway = new Suo_Model_AtmGateway();
        $rooms = $gateway->getRoomsByAtmAddr($addr);
        $rooms_count = count($rooms);
        if ($rooms_count < $page_limit) {
            $rooms_part = $rooms;
        } else {
            $page = (int)$request->getParam('page', 0);
            $page_limit -= 1; // для удобного счёта
            $from = $page * $page_limit;
            if ($rooms_count < $from) {
                $from = 0;
                $page = 0;
            }
            $rooms_part = array_slice($rooms, $from, $page_limit);
            if (count($rooms_part) < $page_limit) {
                $rooms_part = array_merge($rooms_part, array_slice($rooms, 0, $page_limit - count($rooms_part)));
            }
            $rooms_part[] = array('id' => '0', 'number' => '', 'description' => 'Другие', );
        }
        $this->view->page = $page + 1;
        $this->view->rooms = $rooms_part;
    }

    public function checkAction()
    {
        $result = '[]';
        $request = $this->getRequest();
        $room_id = $request->getParam('room');
        if ($room_id) {
            $week_day = $request->getParam('day', '25');
            if ('25' == $week_day) {
                $start_date = date('Y-m-d H:i:s');
            } else { // запись на другой день
                $start_date = "$week_day 07:00:00";
            }
            $addr = $request->getServer('REMOTE_ADDR');
            $gateway = new Suo_Model_AtmGateway();
            $atm = $gateway->getAtmByAddr($addr);
            $params = $atm->addTicket($room_id, $start_date);
            $result = Zend_Json::encode($params);
        }
        echo $result;
        exit();
    }

    public function ticketsAction()
    {
        $request = $this->getRequest();
        $room_id = $request->getParam('room');
        $tickets = array();
        if (!empty($room_id)) {
            $gateway = new Suo_Model_RoomGateway();
            $max_db = $gateway->fetchMaxRecordByRoom($room_id);
            $max = $max_db[0]['recordcount'];
            $start = strtotime('monday this week');
            $today = date('Y-m-d');
            $default = array(
                    'clients' => '',
                    'count' => '0',
                    'max' => $max, );
            for ($i = 0; $i < 13; $i++) {
                $date = date('Y-m-d', strtotime("+$i day", $start));
                $default['clients'] = $this->clientNumToString(0, $date, $today);
                $tickets[$date] = $default;
            }
            $date_start = date('Y-m-d 00:00:00', $start);
            $date_end = date('Y-m-d 23:59:59', strtotime('saturday next week'));
            $ticket_gateway = new Suo_Model_TicketGateway();
            $tickets_db = $ticket_gateway->fetchNumberOfTicketsByRoomAndDate($room_id, $date_start, $date_end);
            foreach ($tickets_db as $ticket) {
                $tickets[$ticket['day']]['clients'] = $this->clientNumToString($ticket['count'], $ticket['day'], $today);
                $tickets[$ticket['day']]['count'] = $ticket['count'];
            }
        }
        $result = Zend_Json::encode($tickets);
        echo $result;
        exit();
    }

    private function clientNumToString($num, $day, $today)
    {
        $result = '';
        $mod = $num % 10;
        if ($day < $today) {
            if (0 == $mod) {
                $result = "Были $num<br />клиентов";
            } else if (1 == $mod) {
                $result = "Был $num<br />клиент";
            } else if ($mod < 5) {
                $result = "Были $num<br />клиента";
            }  else {
                $result = "Были $num<br />клиентов";
            }
        } else {
            if (0 == $mod) {
                $result = "Пока $num<br />клиентов";
            } else if (1 == $mod) {
                $result = "Уже $num<br />клиент";
            } else if ($mod < 5) {
                $result = "Уже $num<br />клиента";
            }  else {
                $result = "Уже $num<br />клиентов";
            }
        }
        return $result;
    }

    public function updateroomsAction()
    {
        $request = $this->getRequest();
        $addr = $request->getServer('REMOTE_ADDR');
        $gateway = new Suo_Model_AtmGateway();
        $rooms = $gateway->getRoomsByAtmAddr($addr);
        $dojoData = new Zend_Dojo_Data('id', $rooms);
        echo $dojoData->toJson();
        exit();
    }

    public function todayrecordsAction()
    {
        $result = array();
        $o_request = $this->getRequest();
        $a_data = Zend_Json::decode($o_request->getRawBody());
        $rooms_id = $a_data['rooms'];
        if ($rooms_id) {
            $gateway = new Suo_Model_TicketGateway();
            $result = $gateway->fetchTodayNumberOfTicketsByRoom($rooms_id);
        }
        echo Zend_Json::encode($result);
        exit();
    }

    public function failAction()
    {
        Suo_Mail::sendTerminalFail($this->getRequest()->getServer('REMOTE_ADDR'));
    }
}