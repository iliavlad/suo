<?php

class Statistic_IndexController extends Suo_Controller_Base
{
    public function indexAction()
    {
        $storeRoom = new Zend_Dojo_View_Helper_DataStore();
        $storeRoom->setView($this->view);
        $this->view->storeRoom = $storeRoom->dataStore('storeRoom',
            'dojox.data.QueryReadStore',
            array('url' => '/suo/statistic/index/room/period/today'));

        $storeOperator = new Zend_Dojo_View_Helper_DataStore();
        $storeOperator->setView($this->view);
        $this->view->storeOperator = $storeOperator->dataStore('storeOperator',
            'dojox.data.QueryReadStore',
            array('url' => '/suo/statistic/index/operator/period/today'));

        $storeTickets = new Zend_Dojo_View_Helper_DataStore();
        $storeTickets->setView($this->view);
        $this->view->storeTickets = $storeTickets->dataStore('storeTickets',
            'dojox.data.QueryReadStore',
            array('url' => '/suo/statistic/index/tickets'));


        $gridRoom = new Zend_Dojo_View_Helper_DataGrid();
        $gridRoom->setView($this->view);
        $gridRoom->dataGrid('gridRoom',
            array(
                'selectionMode' => 'none',
                'style' => 'width: 25em;',
                'store' => 'storeRoom',
                'rowsPerPage' => '100',
                'autoHeight' => 'true',
                'fields' => array(
                    array('field' => 'id', 'label' => 'id', 'options' => array('hidden' => 'true')),
                    array('field' => 'description', 'label' => 'Кабинет', 'options' => array('width' => '200px')),
                    array('field' => 'queue', 'label' => 'Количество клиентов', 'options' => array('width' => 'auto')),
                    )
                ));
        $this->view->gridRoom = $gridRoom;

        $gridOperator = new Zend_Dojo_View_Helper_DataGrid();
        $gridOperator->setView($this->view);
        $gridOperator->dataGrid('gridOperator',
            array(
                'selectionMode' => 'none',
                'style' => 'width: 25em;',
                'store' => 'storeOperator',
                'rowsPerPage' => '100',
                'autoHeight' => 'true',
                'fields' => array(
                    array('field' => 'id', 'label' => 'id', 'options' => array('hidden' => 'true')),
                    array('field' => 'description', 'label' => 'Оператор', 'options' => array('width' => '200px')),
                    array('field' => 'queue', 'label' => 'Количество клиентов', 'options' => array('width' => 'auto')),
                    )
                ));
        $this->view->gridOperator = $gridOperator;

        $date_start = strtotime('monday this week');
        $date_end   = strtotime('sunday this week');

        $fields = array(
                    array('field' => 'id', 'label' => 'id', 'options' => array('hidden' => 'true')),
                    array('field' => 'room', 'label' => 'Кабинет', 'options' => array('width' => '13.5em')),
                    );
        for ($date = $date_start; $date < $date_end; $date += 86400) {
            $fields[] = array('field' => date('Y-m-d', $date),
                              'label' => date('d.m', $date),
                              'options' => array('width' => '3em')
                        );
        }


        $gridTickets = new Zend_Dojo_View_Helper_DataGrid();
        $gridTickets->setView($this->view);
        $gridTickets->dataGrid('gridTickets',
            array(
                'selectionMode' => 'none',
                'style' => 'width: 33em;',
                'store' => 'storeTickets',
                'rowsPerPage' => '100',
                'autoHeight' => 'true',
                'fields' => $fields,
                ));
        $this->view->gridTickets = $gridTickets;
    }

    public function roomAction()
    {
        $request = $this->getRequest();
        $sort = $request->getParam('sort', 'description');
        $period = $request->getParam('period', 'today');
        $gateway = new Suo_Model_Statistic();
        $data = $gateway->getRoomQueues($sort, $period);

        $dojoData = new Zend_Dojo_Data('id', $data);
        echo $dojoData->toJson();
  
        exit();
    }

    public function operatorAction()
    {
        $request = $this->getRequest();
        $sort = $request->getParam('sort', 'description');
        $period = $request->getParam('period', 'today');
        $gateway = new Suo_Model_Statistic();
        $data = $gateway->getOperatorQueues($sort, $period);

        $dojoData = new Zend_Dojo_Data('id', $data);
        echo $dojoData->toJson();

        exit();
    }

    public function ticketsAction()
    {
        $date_start = strtotime('monday this week');
        $date_end   = strtotime('sunday this week');

        $gateway = new Suo_Model_Statistic();
        $tickets_db = $gateway->fetchTicketsGroupedRoomAndDate(date('Y-m-d', $date_start), date('Y-m-d', $date_end));
        $tickets = array();
        $days = array();
        for ($date = $date_start; $date < $date_end; $date += 86400) {
            $days[date('Y-m-d', $date)] = '0';
        }
        foreach ($tickets_db as $ticket) {
            $id = $ticket['id'];
            if (!isset($tickets[$id])) {
                $tickets[$id] = $days;
                $tickets[$id]['id'] = $id;
                $tickets[$id]['room'] = $ticket['room'];
            }
            $tickets[$id][$ticket['day']] = $ticket['count'];
        }
        $dojoData = new Zend_Dojo_Data('id', $tickets);
        echo $dojoData->toJson();
        exit();
    }

}