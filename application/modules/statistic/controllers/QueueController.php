<?php

class Statistic_QueueController extends Suo_Controller_Base
{
    public function indexAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $tickets = array();
            $post = $request->getPost();
            if (isset($post['tickets'])) {
                $tickets = $post['tickets'];
            } else {
                foreach ($post as $param => $value) {
                    $base = 'decline';
                    $pos = strpos($param, $base);
                    if (false !== $pos) {
                        $tickets = array(substr($param, strlen($base)));
                        break;
                    }
                }
            }
            if (!empty($tickets)) {
                $ticket_gateway = new Suo_Model_TicketGateway();
                $ticket_gateway->declineTickets($tickets);
            }
        }
        $gateway = new Suo_Model_Statistic();
        $this->view->tickets = $gateway->getTicketsToDecline();
    }

}