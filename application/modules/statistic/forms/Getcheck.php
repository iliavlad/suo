<?php

class Statistic_Form_Getcheck extends Zend_Dojo_Form
{
     /**
     * Form initialization
     *
     * @return void
     */
    public function init()
    {
        $this
            ->setMethod(Zend_Form::METHOD_POST)
            ->setAttribs(
                array(
                    'id'   => 'formGetACheck',
                    'name' => 'formGetACheck'
                )
            );

        $this->addElement(
                'SubmitButton',
                'noCheck',
                array(
                    'dijitParams'    => array(
                        'jsId'           => 'noCheck',
                        'required'       => false,
                        'label'          => 'нет талона',//Если нет талона, нажмите кнопку',
                    )
                )
        );

        $this->setElementDecorators(
            array(
                array('DijitElement'),
          //      array('Errors'),
          //      array('Description'),
          //      array('Label', array('separator' => ' ')),
                array('HtmlTag', array('tag' => 'div', 'class' => 'elementGroup clear')),
            ),
            array(),
            false
        );
    }

    /**
     * Form initialization
     *
     * @return void
     */
/*    public function init()
    {
        $this
            ->setMethod(Zend_Form::METHOD_POST)
            ->setAttribs(
                array(
                    'id'   => 'dialogExample',
                    'name' => 'dialogExample',
                    'jsId' => 'dialogExample'
                )
            );

        $this->clearDecorators();
        $this->addDecorator('FormElements')
             ->addDecorator('Dialog', array(
                 'title' => 'dijit.Dialog as Dojo Form\'s Decorator'
             ));

        $this
            ->addElement(
                'ValidationTextBox',
                'mailSiteName',
                array(
                    'label'          => 'mail-site-name',
                    'optionalSuffix' => ':',
                    'dijitParams'    => array(
                        'jsId'           => 'mailSiteName',
                        'maxLength'      => 255,
                        'required'       => true
                    )
                )
            );

        $this->setElementDecorators(
            array(
                array('DijitElement'),
                array('Errors'),
                array('Description'),
                array('Label', array('separator' => ' ')),
                array('HtmlTag', array('tag' => 'div', 'class' => 'elementGroup clear')),
            ),
            array(),
            false
        );
    }*/
}