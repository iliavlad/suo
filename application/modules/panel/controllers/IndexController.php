<?php

class Panel_IndexController extends Suo_Controller_Base
{
    public function indexAction()
    {
        $request = $this->getRequest();
        $addr = $request->getServer('REMOTE_ADDR');
        $this->view->norooms = true;
        $gateway = new Suo_Model_PanelGateway();
        $panel = $gateway->getPanelByAddr($addr);
        if ($panel) {
            $room_gateway = new Suo_Model_RoomGateway();
            $rooms_db = $room_gateway->fetchRoomIdByPanelAddress($addr);
            if (count($rooms_db) > 0) {
                $this->view->norooms = false;
                $rooms = array();
                foreach ($rooms_db as $room) {
                    $rooms[] = $room['room_id'];
                }
                $rooms = "" . implode(",", $rooms) . "";
                $this->getResponse()->setHeader('Set-Cookie',
                        "rooms=$rooms;path=/");
                $this->getResponse()->setHeader('Set-Cookie',
                        "panel=" . $panel->id . ";path=/");
			}
        }
    }

    public function listAction()
    {
        $request = $this->getRequest();
        $room_ids = $request->getCookie('rooms');
		//throw new Exception(var_export($request->getCookie(), true));
        if (empty($room_ids)) {
            exit();
        }
        $this->_helper->layout->disableLayout();

        // ������������� ������� ������� �� ��������� ������ � ��������
        $s_ring_tickets = $request->getParam('ringTickets');
        if ($s_ring_tickets && '0' != $s_ring_tickets) {
            $a_ticket_id = explode(',', $s_ring_tickets);
            $request = $this->getRequest();
            $panel_id = $request->getCookie('panel');
            $ticket_gateway = new Suo_Model_TicketGateway();
            $ticket_gateway->setManyTicketsStatusByPanel($panel_id, $a_ticket_id, Suo_Model_Ticket::WAITCLIENT);
        }

        $this->view->rooms = array();
        $room_gateway = new Suo_Model_RoomGateway();
        $check_gateway = new Suo_Model_CheckGateway();

        $rooms_db = $room_gateway->fetchRoomsByIds($room_ids);

        $rooms = array();

        $today = date('Y-m-d');
        $date_start = $today . ' 00:00:00';
        $date_end   = $today . ' 23:59:59';

        $a_ring_tickets = array();

        foreach ($rooms_db as $room) {
            $room['check'] = array();
            $room['wait'] = false;
            $room['current_client'] = '';

            if ($room['accept'] != '0' || $room['acceptday'] != $today) {
                $room['accept'] = '1';

                $checks_db = $check_gateway->fetchChecksByRoomIdAndDates($room['id'], $date_start, $date_end);
                foreach ($checks_db as $check) {
                    if ('' == $room['current_client']) {
                        if (Suo_Model_Ticket::ACCEPTED == $check['status_id']) {
                            $room['current_client'] = $check['code'];
                        } else if (Suo_Model_Ticket::WAITCLIENT == $check['status_id']) {
                            $room['wait'] = true;
                            $room['current_client'] = $check['code'];
                        } else if (Suo_Model_Ticket::WAITANDRING == $check['status_id']) {
                            $room['wait'] = true;
                            $room['current_client'] = $check['code'];
                            $a_ring_tickets[] = $check['id'];
                        }
                    }

                    $room['check'][] = $check;
                }
            }

            $rooms[] = $room;
        }

        $this->view->rooms = $rooms;
		$this->view->a_ring_tickets = $a_ring_tickets;
    }
}