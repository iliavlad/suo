<?php

class Operator_Form_Ticket extends Zend_Form
{
    public function init()
    {
        $this->addElement('text', 'employee_id', array('label' => 'operator', ));
        $this->addElement('text', 'room_id', array('label' => 'room', ));
        $this->addElement('text', 'check_id', array('label' => 'check', ));
        $this->addElement('text', 'priority', array('label' => 'priority', ));
        $this->addElement('text', 'ticket_kit_id', array('label' => 'ticket_kit', ));
        $this->addElement('submit', 'submit', array('label' => 'submit', 'ignore' => true, ));
        $this->setMethod('post');
        $this->setAction('');
    }
}