<?php

class Operator_Form_Login extends Zend_Form
{
    public function init()
    {
        $user = new Zend_Form_Element_Select('id',
                array('label' => 'Выберите пользователя', 'onchange' => 'onUserChange(this.value); return false;'));

        $operator_gateway = new Suo_Model_OperatorGateway();
        $all_operator = $operator_gateway->fetchAllWithRoomNumber();

        $multi_rooms = array();

        foreach ($all_operator as $operator) {
            $id = $this->getIdFromRequest($operator['id'], $operator['room_id'], $operator['room_number']);
            $user->addMultiOption(
                    $id,
                    'Кабинет ' . $operator['room_number'] . ' ' .
                        $operator['last_name'] . ' ' .
                        mb_substr($operator['first_name'], 0, 1, 'UTF-8') . '. ' .
                        mb_substr($operator['parent_name'], 0, 1, 'UTF-8') . '.');
            if ($operator['windowcount'] > 1) {
                $multi_rooms[$id] = $operator['windowcount'];
            }
        }

        $this->_user_select = $user;
        $this->addElement($user);

        foreach ($multi_rooms as $id => $count) {
            $room = new Zend_Form_Element_Select('window' . $id, array('class' => 'window', 'ignore' => true, ));
            $room->removeDecorator('label');
            for ($i = 1; $i <= $count; $i++) {
                $room->addMultiOption($i, 'Окно ' . $i);
            }
            $this->addElement($room);
        }
        $this->_multi_room_user = $multi_rooms;

        $this->addElement('text', 'pincode',
            array('label' => 'Введите код', 'class' => 'pincode', 'id' => 'focusFirst', ));
        $this->addElement('submit', 'submit', array('label' => 'Войти', 'ignore' => true, 'class' => "mainbutton", ));
        $this->setMethod('post');
        $this->setAttrib('id', 'form');
    }

    public function setOperator($operator_id)
    {
        $this->_user_select->setValue($operator_id);
    }

    public function getIdFromRequest($operator_id, $room_id, $room_number)
    {
        return $operator_id . '-' . $room_id . '-' . $room_number;
    }

    public function getOperatorIdFromFormValue()
    {
        return $this->_form_value_operator_id;
    }

    public function getRoomIdFromFormValue()
    {
        return $this->_form_value_room_id;
    }

    public function getRoomNumberFromFormValue()
    {
        return $this->_form_value_room_number;
    }

    public function parseFormIdValue($value)
    {
        if (empty($this->_form_value_operator_id)) {
            $a = explode('-', $value);
            $this->_form_value_operator_id = $a[0];
            $this->_form_value_room_id = $a[1];
            $this->_form_value_room_number = $a[2];
        }
    }

    public function getMultiRoomUser()
    {
        return $this->_multi_room_user;
    }

    /**
     *
     * @var Zend_Form_Element_Select
     */
    private $_user_select = null;

    private $_form_value_operator_id = '';

    private $_form_value_room_id = '';

    private $_form_value_room_number = '';

    private $_multi_room_user = '';
}