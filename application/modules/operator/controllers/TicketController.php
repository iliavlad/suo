<?php

class Operator_TicketController extends Suo_Controller_Operator_Base
{
    public function byclientAction()
    {
        $data = new Zend_Dojo_Data('id', $this->getTickets());
        echo $data->toJson();
        exit();
    }

    public function setmaxrecordsAction()
    {
        $o_request = $this->getRequest();
        $n_records = $o_request->getParam('records');
        if ($n_records) {
            $suo_operator_session = new Zend_Session_Namespace('Suo_Operator');
            $room_id = $suo_operator_session->room_id;

            $o_room_gateway = new Suo_Model_RoomGateway();
            $o_room_gateway->setMaxTodayRecords($room_id, $n_records);
        }
        exit();
    }
}
