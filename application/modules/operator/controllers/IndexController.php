<?php

class Operator_IndexController extends Suo_Controller_Base
{
    public function indexAction()
    {
        // форма логина будет выводится без layout
        $this->_helper->layout->disableLayout();

        $request = $this->getRequest();
        $form = new Operator_Form_Login();

        $operator_id = $request->getCookie('operator', null);
        if (null != $operator_id) {
            $form->setOperator($operator_id);
        }
        $form->setAction($this->view->baseUrl('operator'));
        $this->view->form = $form;
        $this->view->incorrect_login = false;
        $multi_room_user = array_keys($form->getMultiRoomUser());
        $this->view->multi_room_user = Zend_Json::encode($multi_room_user);
        $request = $this->getRequest();
        if ($request->isPost()) {
            if ($form->isValid($request->getPost())) {
                $params = $form->getValues();
                $form->parseFormIdValue($form->getValue('id'));
                $params['id'] = $form->getOperatorIdFromFormValue();
                $adapter = $this->getAuthAdapter($params);
                $auth = Zend_Auth::getInstance();
                $auth_result = $auth->authenticate($adapter);
                if (!$auth_result->isValid()) {
                    $this->view->incorrect_login = true;
                } else {
                    $form_id = $form->getValue('id');
                    $this->getResponse()->setHeader('Set-Cookie',
                            'operator=' . $form_id .
                            ';expires=' . date('l, d-M-y H:i:s e', time() + 60*60*24*7) . ';' .
                            'path=/');
                    $suo_operator_session = new Zend_Session_Namespace('Suo_Operator');
                    $room_id = $form->getRoomIdFromFormValue();
                    $suo_operator_session->room_id = $room_id;
                    $suo_operator_session->room_number = $form->getRoomNumberFromFormValue();
                    $room_window = '1';
                    if (in_array($form_id, $multi_room_user)) {
                        $param_id = 'window' . str_replace('-', '', $form_id);
                        $room_window = $form->getValue($param_id);
                    }
                    $suo_operator_session->room_window = $room_window;

                    // проверяем, была ли выбрана опция "Завершить приём на сегодня"
                    $room_gateway = new Suo_Model_RoomGateway();
                    $room = $room_gateway->fetchById($room_id);
					$suo_operator_session->max_today_records = $room->max_today_records;
                    if ('0' != $room->accept) {
                        return $this->_helper->redirector('screen', 'call', 'operator');
                    } else {
                        return $this->_helper->redirector('index', 'resumption', 'operator');
                    }
                }
            }
        }
    }

    public function logoutAction()
    {
        Zend_Auth::getInstance()->clearIdentity();
        return $this->_helper->redirector('index');
    }

    public function stopAction()
    {
        $request = $this->getRequest();
        $operator = $request->getCookie('operator', null);
        if (!empty($operator)) {
            $a = explode('-', $operator);
            $room_id = $a[1];
            $gateway = new Suo_Model_OperatorGateway();
            $gateway->stopAccept($room_id);
            echo $room_id;
        }
        exit();
    }

    private function getAuthAdapter($data)
    {
        $gateway = new Suo_Model_OperatorGateway();
        $operator = $gateway->create($data);
        return $operator;
    }

}