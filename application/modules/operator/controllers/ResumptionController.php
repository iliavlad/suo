<?php

class Operator_ResumptionController extends Suo_Controller_Base
{
    public function indexAction()
    {
        $this->_helper->layout->disableLayout();
        $request = $this->getRequest();
        $resumption = $request->getParam('resumption', '0');
        if ('1' == $resumption) {
            $suo_operator_session = new Zend_Session_Namespace('Suo_Operator');
            $room_id = $suo_operator_session->room_id;
            $room_gateway = new Suo_Model_RoomGateway();
            $room = $room_gateway->setRoomAccept($room_id, '1');
            return $this->_helper->redirector('screen', 'call', 'operator');
        }
    }
}
