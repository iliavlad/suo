<?php

class Operator_StartController extends Suo_Controller_Operator_Base
{
    public function screenAction()
    {
        $request = $this->getRequest();
        $this->view->current_check_code = $request->getParam('code');
        $this->view->current_ticket_id = $request->getParam('ticket');
        //var_dump($request->getParam('ticket'));
    }

    public function indexAction()
    {
        $request = $this->getRequest();
        $code = $request->getParam('code');
        $ticket_id = $request->getParam('ticket');

//        $current_ticket_id = $request->getParam('current');
//        if (!empty($current_ticket_id)) {
//            $this->_ticket_gateway->setTicketStatus($this->_operator_id,
//                    $current_ticket_id, Suo_Model_Ticket::INQUEUE);
//        }

        $this->_ticket_gateway->setTicketStatus($this->_operator_id, $ticket_id, Suo_Model_Ticket::ACCEPTED);

//        if (empty($code)) {
//            foreach ($this->_tickets as $ticket) {
//                if ($ticket['id'] === $ticket_id) {
//                    $code = $ticket['code'];
//                    break;
//                }
//            }
//        }

        return $this->_helper->redirector('screen', 'end', 'operator',
            array('ticket' => $ticket_id, 'code' => $code, ));
    }
}