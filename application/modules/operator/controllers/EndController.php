<?php

class Operator_EndController extends Suo_Controller_Operator_Base
{
    public function screenAction()
    {
        $request = $this->getRequest();
        $this->view->current_check_code = $request->getParam('code');
        $this->view->current_ticket_id = $request->getParam('ticket');

        $room_gateway = new Suo_Model_RoomGateway();
        $rooms = $room_gateway->fetchAllToOperator();

        $this->view->rooms = $rooms;
    }

    public function indexAction()
    {
        $request = $this->getRequest();
        $ticket_id = $request->getParam('ticket');

        $room_redirect = $request->getParam('redirect', '0');

        if ('0' == $room_redirect) {
            $this->_ticket_gateway->setTicketStatus($this->_operator_id, $ticket_id, Suo_Model_Ticket::CLOSED);
        } else {
            /**
             * @var Suo_Model_Ticket
             */
            $ticket = $this->_ticket_gateway->fetchById($ticket_id);
            $ticket->redirectToRoom($room_redirect, $this->_operator_id);
        }

        return  $this->_helper->redirector('screen', 'call', 'operator');
    }
}
