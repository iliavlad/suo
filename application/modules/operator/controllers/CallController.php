<?php
class Operator_CallController extends Suo_Controller_Operator_Base
{
    public function screenAction()
    {

    }

    public function indexAction()
    {
        $tickets = $this->getTickets();
        if (0 == count($tickets)) { // если заявок нет, то переходим к экрану вызова
            return $this->_helper->redirector('screen', 'call', 'operator');
        } else {
            $request = $this->getRequest();
            $ticket_str = $request->getParam('ticket');
            if (empty($ticket_str)) { // нажали кнопку "Вызвать"
                $ticket = $tickets[0]['id'];
                $code = $tickets[0]['code'];
            } else { // нажали на вызов клиента без очереди
                $ticket_data = explode('-', $ticket_str);
                $ticket = $ticket_data[0];
                $code = $ticket_data[1];
                $current_ticket_id = $request->getParam('current');
                if (!empty($current_ticket_id)) {
                    $this->_ticket_gateway->setTicketStatus($this->_operator_id,
                            $current_ticket_id, Suo_Model_Ticket::INQUEUE);
                }
            }

            // устанавливаем статус "ждем клиента" и переходим к экрану ожидания
            $this->_ticket_gateway->setTicketStatus($this->_operator_id, $ticket, Suo_Model_Ticket::WAITCLIENT);
            return $this->_helper->redirector('screen', 'start', 'operator',
                array('ticket' => $ticket, 'code' => $code, ));
        }
    }

    public function nextAction()
    {
        $request = $this->getRequest();
        $ticket_id = $request->getParam('ticket');

        $this->_ticket_gateway->updateTicketPosition($this->_operator_id, $ticket_id, 3);

        $tickets = $this->getTickets(true);
        $ticket = $tickets[0]['id'];
        $code = $tickets[0]['code'];
        $this->_ticket_gateway->setTicketStatus($this->_operator_id, $ticket, Suo_Model_Ticket::WAITCLIENT);
        return $this->_helper->redirector('screen', 'start', 'operator',
                array('ticket' => $ticket, 'code' => $code, ));
    }

}