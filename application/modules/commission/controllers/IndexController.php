<?php

class Commission_IndexController extends Suo_Controller_Base
{
    public function indexAction()
    {
        $this->_forward('index', 'timetable');
    }
}