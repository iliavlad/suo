<?php

class Commission_TimetableController extends Suo_Controller_Base
{
    public function indexAction()
    {
        $week = 'current';
        $room = '1';
        return $this->_helper->redirector('set', null, null, array('week' => $week, 'room' => $room, ));
    }

    public function setAction()
    {
        $request = $this->getRequest();
        $week = $request->getUserParam('week', 'current');
        $room = $request->getUserParam('room', '1');
        $week_links = $this->getWeekLinks($week);
        $this->view->week_links = $week_links;
        $this->view->week_links_current = $week;

        $room_gateway = new Suo_Model_RoomGateway();
        $rooms = $room_gateway->fetchAll();

        $timetable_model = new Commission_Model_Timetable();
        $timetable_model->setWeek($week);
        $timetable_model->setRoom($room);

        if ($request->isPost()) {
            $timetable_model->save($request->getParams());
            return $this->_helper->redirector('set', null, null, array('week' => $week, 'room' => $room, ));
        }

        $form = $timetable_model->getForm();
        $action = $this->view->url(array('week' => $week, 'room' => $room, ), 'timetable');
        $form->setAction($action);
        $this->view->timetable_form = $form;

        $this->view->room_current = $room;
        $this->view->rooms = $rooms;
    }

    private function getWeekLinks($start)
    {
        $links = array();
        $week = 0;
        $caption_prev = 'minus1';
        $caption_current = 'current';
        $caption_next = 'plus1';
        if (false !== strpos($start, 'minus')) {
            $number = substr($start, strlen('minus'));
            if (!is_numeric($number)) {
                $start = 'current';
            } else {
                $number = intval($number);
                $week = -$number;
            }
            if (1 == $number) {
                $caption_next = 'current';
            } else {
                $caption_next = 'minus' . ($number - 1);
            }
            $caption_current = 'minus' . $number;
            $caption_prev = 'minus' . ($number + 1);
        } else if (false !== strpos($start, 'plus')) {
            $number = substr($start, strlen('plus'));
            if (!is_numeric($number)) {
                $start = 'current';
            } else {
                $number = intval($number);
                $week = $number;
            }
            if (1 == $number) {
                $caption_prev = 'current';
            } else {
                $caption_prev = 'plus' . ($number - 1);
            }
            $caption_current = 'plus' . $number;
            $caption_next = 'plus' . ($number + 1);
        } else {
            $start = 'current';
        }
        if ('current' == $start) {
            $week = 0;
        }
        $week_day = intval(date('w'));
        if (0 == $week_day) {
            $week_day = 6;
        } else {
            --$week_day;
        }

        // current
        $week_start_current = strtotime("$week week -$week_day day");
        $week_end_current   = strtotime("$week week +" . (7 - $week_day - 1) . " day");

        // prev
        $week_start_prev = strtotime("-7 day", $week_start_current);
        $week_end_prev   = strtotime("-7 day", $week_end_current);

        // next
        $week_start_next = strtotime("+7 day", $week_start_current);
        $week_end_next   = strtotime("+7 day", $week_end_current);

        $format = 'd.m.y';
        $week_caption_current = date($format, $week_start_current) . ' - ' . date($format, $week_end_current);
        $week_caption_prev = date($format, $week_start_prev) . ' - ' . date($format, $week_end_prev);
        $week_caption_next = date($format, $week_start_next) . ' - ' . date($format, $week_end_next);

        $links = array($caption_prev => $week_caption_prev, $caption_current => $week_caption_current, $caption_next => $week_caption_next, );
        return $links;
    }
}