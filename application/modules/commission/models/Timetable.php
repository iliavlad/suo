<?php

class Commission_Model_Timetable
{
    public function getForm()
    {
        throw new Exception('not implemented');
        if (null == $this->_form) {
            $room_gateway = new Suo_Model_RoomGateway();
            $room = $room_gateway->fetchById($this->_room);

            /* @var $operator_set Suo_Model_OperatorSet */
            $operator_value = $room->operator;
            $operator_select_options = array(-1 => 'Не выбран', );
            foreach ($operator_value as $operator) {
                $operator_select_options[$operator->id] = $operator->__toString();
            }

            $date_start = date("Y-m-d 00:00:00", $this->_date_start);
            $date_end = date("Y-m-d 23:59:59", $this->_date_end);

            $workday_gateway = new Suo_Model_WorkdayGateway();
            $timetable_base = $workday_gateway->fetchByRoomAndDates($this->_room, $date_start, $date_end);
            $timetable = array();
            $timetable_save = array();
            $date_start = $this->_date_start;
            $format_name = 'Ymd';
            foreach ($timetable_base as $row) {
                $begin = strtotime($row['begin']);
                $end = strtotime($row['end']);
                $key = date($format_name, $begin) . '_1_operator';
                $timetable_save[$key] = $row;
                $key = date($format_name, $begin) . '_1_';
                $timetable[$key] = array('operator' => $row['employee_id'],
                    'from' => date('H', strtotime($row['begin'])),
                    'till' => date('H', strtotime($row['end'])),
                    'from_minutes' => date('i', strtotime($row['begin'])),
                    'till_minutes' => date('i', strtotime($row['end'])),
                );
            }
            $this->_timetable = $timetable_save;

            $form = new Commission_Form_Timetable();
            $form->removeDecorator('htmltag');
            $translator = $form->getTranslator();
            $days = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday', );
            $hours = array('08-09', '09-10', '10-11', '11-12', '12-13', '13-14', '14-15', '15-16', '16-17', '17-18', '18-19', '19-20', );
            $groups_week = array();

            $format_day = 'd.m';
            $i = 0;

            $hours = array(8 => 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, );
            $range_minutes = range(0, 55, 5);
            $minutes = array_combine($range_minutes, $range_minutes);


            foreach ($days as $day) {
                $legend_day = date($format_day, strtotime("+$i day", $date_start));
                $group = new Zend_Form_DisplayGroup($day, $form->getPluginLoader(Zend_Form::DECORATOR),
                    array('legend' => $translator->translate($day) . ' ' . $legend_day, 'class' => 'daygroup', ));
                $group->removeDecorator('htmltag');
                $group->removeDecorator('dtddwrapper');
                $select_name_date = date($format_name, strtotime("+$i day", $date_start));

                $element_name = $select_name_date . '_1_';
                $element = new Suo_Form_Element_Select($element_name . 'operator', array('label' => '', ));
                $element->addMultiOptions($operator_select_options);
                if (isset($timetable[$element_name])) {
                    $value = $timetable[$element_name]['operator'];
                } else {
                    $value = '-1';
                }
                $element->setValue($value);
                $group->addElement($element);

                $element = new Suo_Form_Element_Select($element_name . 'from', array('label' => 'С', ));
                $element->addMultiOptions($hours);
                if (isset($timetable[$element_name])) {
                    $value = $timetable[$element_name]['from'];
                } else {
                    $value = '8';
                }
                $element->setValue($value);
                $group->addElement($element);

                $element = new Suo_Form_Element_Select($element_name . 'from_minutes');
                $element->addMultiOptions($minutes);
                if (isset($timetable[$element_name])) {
                    $value = $timetable[$element_name]['from_minutes'];
                } else {
                    $value = $minutes[0];
                }
                $element->setValue($value);
                $group->addElement($element);

                $element = new Suo_Form_Element_Select($element_name . 'till', array('label' => 'По', ));
                $element->addMultiOptions($hours);
                if (isset($timetable[$element_name])) {
                    $value = $timetable[$element_name]['till'];
                } else {
                    $value = '23';
                }
                $element->setValue($value);
                $group->addElement($element);

                $element = new Suo_Form_Element_Select($element_name . 'till_minutes');
                $element->addMultiOptions($minutes);
                if (isset($timetable[$element_name])) {
                    $value = $timetable[$element_name]['till_minutes'];
                } else {
                    $value = $minutes[0];
                }
                $element->setValue($value);
                $group->addElement($element);

//                $element_name = $select_name_date . '_2_';
//                $element = new Suo_Form_Element_Select($element_name . 'operator', array('label' => '', ));
//                $element->addMultiOptions($operator_select_options);
//                $value = '-1';
//                $element->setValue($value);
//                $group->addElement($element);
//
//                $element = new Suo_Form_Element_Select($element_name . 'from', array('label' => 'С', ));
//                $element->addMultiOptions($hours);
//                $group->addElement($element);
//
//                $element = new Suo_Form_Element_Select($element_name . 'till', array('label' => 'По', ));
//                $element->addMultiOptions($hours);
//                $group->addElement($element);

                $groups_week[$day] = $group;
                ++$i;
            }
            $form->addDisplayGroups($groups_week);
            $this->_form = $form;
        }
        return $this->_form;
    }

    public function setRoom($room)
    {
        $this->_room = $room;
        return $this;
    }

    public function setWeek($start)
    {
        $week = 0;
        if (false !== strpos($start, 'minus')) {
            $number = substr($start, strlen('minus'));
            if (!is_numeric($number)) {
                $start = 'current';
            } else {
                $number = intval($number);
                $week = -$number;
            }
        } else if (false !== strpos($start, 'plus')) {
            $number = substr($start, strlen('plus'));
            if (!is_numeric($number)) {
                $start = 'current';
            } else {
                $number = intval($number);
                $week = $number;
            }
        } else {
            $start = 'current';
        }
        if ('current' == $start) {
            $week = 0;
        }

        $week_day = intval(date('w'));
        if (0 == $week_day) {
            $week_day = 6;
        } else {
            --$week_day;
        }

        // current
        $week_start = strtotime("$week week -$week_day day");
        $week_end   = strtotime("$week week +" . (7 - $week_day - 1) . " day");

        $this->_date_start = $week_start;
        $this->_date_end = $week_end;

        $this->_week = $start;

        return $this;
    }

    /**
     * Сохранение данных формы расписания
     *
     * @param array $form_params
     */
    public function save($form_params)
    {
        $workday_gateway = new Suo_Model_WorkdayGateway();
        $skipped = array('week', 'room', 'module', 'controller', 'action', 'submit');
        if (null == $this->_form) {
            $this->getForm();
        }
        $timetable = $this->_timetable;
        $workday_gateway->deleteWorkByRoomAndDates($form_params['room'],
            date("Y-m-d 00:00:00", $this->_date_start),
            date("Y-m-d 23:59:59", $this->_date_end));
        foreach ($form_params as $key => $value) {
            if (!in_array($key, $skipped) && strpos($key, 'operator') && $value != '-1') {
                $element_name = substr($key, 0, strpos($key, 'operator'));
                $datetime = explode('_', $key);
                if (count($datetime) == 3) {
                    $time = str_split($datetime[1], 2);
                    $date = strtotime($datetime[0]);
                    $begintime = strtotime("+" . $form_params[$element_name . 'from'] . " hour " .
                                           "+" . $form_params[$element_name . 'from_minutes'] . " minute", $date);
                    $endtime   = strtotime("+" . $form_params[$element_name . 'till'] . " hour " .
                                           "+" . $form_params[$element_name . 'till_minutes'] . " minute", $date);
                    $date_start = date('Y-m-d H:i:s', $begintime);
                    $date_end   = date('Y-m-d H:i:s', $endtime);

                    $workday_params = array(
                        'room_id' => $form_params['room'], 'employee_id' => $value, 'begin' => $date_start, 'end' => $date_end, );
                    $workday = $workday_gateway->create($workday_params);
                    $workday->save();
                }
            }
        }
    }

    /**
     * @var Commission_Form_Timetable
     */
    protected $_form = null;

    protected $_room = null;

    protected $_week = null;

    protected $_date_start = null;

    protected $_date_end = null;

    protected $_timetable = null;
}