<?php

class LoginController extends Suo_Controller_Base
{
    public function indexAction()
    {
        $form = new Suo_Form_Login();
        $this->view->form = $form;
        $this->view->error_message = '';
        $request = $this->getRequest();
        if ($request->isPost()) {
            if ($form->isValid($request->getPost())) {
                $adapter = $this->getAuthAdapter($form->getValues());
                $auth = Zend_Auth::getInstance();
                $auth_result = $auth->authenticate($adapter);
                if (!$auth_result->isValid()) {
                    $this->view->error_message = 'Попробуйте снова';
                } else {
                    if ('1' == $adapter->id) {
                        $module = 'admin';
                    } else {
                        $module = 'operator';
                    }
                    return $this->_helper->redirector('index', 'index', $module);
                }
            }
        }
    }

    public function logoutAction()
    {
        Zend_Auth::getInstance()->clearIdentity();
        return $this->_helper->redirector('index');
    }

    private function getAuthAdapter($data)
    {
        $gateway = new Suo_Model_OperatorGateway();
        $data['id'] = '1';
        $operator = $gateway->create($data);
        return $operator;
    }
}