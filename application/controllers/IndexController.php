<?php

class IndexController extends Suo_Controller_Base
{
    public function indexAction()
    {
        $auth = Zend_Auth::getInstance();
        if (!$auth->hasIdentity()) {
            return $this->_helper->redirector('index', 'login');
        } else {
            $operator = $auth->getIdentity();
            $id = $operator->id;
            if ('1' == $id) {
                $module = 'admin';
            } else {
                $module = 'operator';
                return $this->_helper->redirector('index', 'index', $module);
            }
            //return $this->_helper->redirector('index', 'index', $module);
        }
    }
}